"""kernel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include

from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    re_path('^', include('pages.urls')),
    path('sos7243/', admin.site.urls),
    path('sos7243/book/', include('django.contrib.admindocs.urls')),
    re_path(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    re_path(r'^', include("django_prometheus.urls")),
    re_path(r'^watchman/', include('watchman.urls')),
    re_path(r'hitcount/', include('hitcount.urls', namespace='hitcount')),
    re_path(r'^sos7243/defender/', include('defender.urls')),
    re_path(r'^ckeditor/', include('ckeditor_uploader.urls')),
    re_path(r'^accounts/', include("django.contrib.auth.urls")),
    re_path(r'^accounts/', include('accounts.urls')),
    re_path(r'^dashboard/', include("dashboard.urls")),
    re_path(r'^api/', include("api.urls")),
    re_path(r'^blog/', include("blog.urls")),
    # re_path(r'^profiling/silk/7243', include('silk.urls', namespace='silk')),
    re_path(r'^rss/', include('api.v1.feed.urls')),
    re_path(r'^project/', include("project.urls"))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns