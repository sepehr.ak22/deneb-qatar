from django import forms
from .models import Ticket
from .models import Comments

from accounts.models import User

class TicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ('subject', 'priority_status', 'department_id', 'message',)

class TicketReplyForm(forms.ModelForm):

    class Meta:
        model = Comments
        fields = ('message', 'staff_id', 'ticket_id', 'parent', 'answer_status')