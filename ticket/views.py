from django.shortcuts import render, get_object_or_404
from django.http import Http404
from django.views.generic import ListView
from django.views.generic import View
from django.views.generic import DetailView
from django.views.generic import TemplateView
from django.views.generic import CreateView

from django.contrib.messages.views import SuccessMessageMixin

from .models import Ticket
from .forms import TicketForm
from .forms import TicketReplyForm

from pages.models import Site


class TicketCreateView(SuccessMessageMixin, CreateView):
    model = Ticket
    form = TicketForm
    success_message = 'Your ticket raised successfully. Please check left side in `Previous Ticket` and track your ticket.'
    fields = ('subject', 'priority_status', 'department_id', 'message',)
    template_name = "dashboard/pages/ticket.html"
    page_name = 'Ticket'
    context_object_name = 'tickets'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tickets"] = Ticket.objects.filter(raised_by = self.request.user.pk)
        context["site"] = Site.objects.first()
        return context
    
    def form_valid(self, form):
        form.instance.raised_by = self.request.user
        return super().form_valid(form)

class TicketDetailView(View):
    template_name = "dashboard/pages/ticket-detail.html"
    view = {
        'page_name': 'Ticket',
        'remove_footer': True
    }

    def get(self, request, sku, *args, **kwargs):
        ticket = get_object_or_404(Ticket, sku=sku)
        form = TicketReplyForm()
        if ticket.raised_by == request.user:
            return render(request, self.template_name, {
                'ticket': ticket, 
                'view': self.view, 
                'form': form,
                'site': Site.objects.first()
            })
        else:
            # print(form.errors)
            raise Http404('Ticket Not Found')
            
        
        
    def post(self, request, sku, *args, **kwargs):
        ticket = get_object_or_404(Ticket, sku=sku)
        
        if request.POST['answer_status'] == 'a' and int(request.POST['staff_id']) == request.user.pk and int(request.POST['ticket_id']) == ticket.pk and int(request.POST['parent_id']) == ticket.pk:
            form = TicketReplyForm(request.POST)
            if form.is_valid():
                ticket.answer_status = 'u'
                form.save()
                ticket.save()
        # print(form.errors)
        clean_form = TicketReplyForm()
        return render(request, self.template_name, {
            'ticket': ticket, 
            'view': self.view, 
            'form': clean_form,
            'site': Site.objects.first()
            })
