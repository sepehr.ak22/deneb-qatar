from django.dispatch import receiver

from django.db.models.signals import post_save

from django.core.mail import send_mail

from django.conf import settings

from ticket.models import (
    Department,
    Ticket,
    Comments
)


@receiver(post_save, sender=Ticket)
def massive_mailing_list(sender, instance, created, **kwargs):

    if created:
        
        department_users = [user for user in instance.department_id.users.all()]
        
        if department_users:

            send_email(department_users)
    

@receiver(post_save, sender=Comments)
def change_ticket_status(sender, instance, created, **kwargs):

    if created:

        department_users = [user for user in instance.ticket_id.department_id.users.all()]
        
        if instance.staff_id in department_users:

            ticket = instance.ticket_id
            ticket.answer_status = 'a'
            ticket.save()

            send_email([instance.staff_id])

        if instance.staff_id == instance.ticket_id.raised_by:

            ticket = instance.ticket_id
            ticket.answer_status = 'u'
            ticket.save()

            send_email(department_users)

        
def send_email(users):

    for user in users:

        send_mail(
                'Subject',
                'Here is the message.',
                settings.EMAIL_HOST_USER,
                [user.email],
                fail_silently=False,
                )