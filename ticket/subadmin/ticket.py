from django.contrib import admin

from ticket.models import Ticket
from .general import GeneralAdminDashboardDesign
from ticket.actions.ticket import make_Answered, make_Unanswered


@admin.register(Ticket)
class TicketAdmin(GeneralAdminDashboardDesign):
    search_fields = ('subject', 'sku',)
    list_filter = ('created', 'department_id', 'answer_status', 'priority_status')
    list_display = ('sku', 'subject', 'priority_status', 'raised_by', 'is_answered', 'department_id', 'created')
    actions = (make_Answered, make_Unanswered)
    

    fieldsets = (
        (None, {
            'fields': (
                ('subject',),
                ('message', ),
            ),
        }),
        ('Advanced Options', {
            'classes': ('collapse', ),
            'fields': (
                ('answer_status', 'priority_status', 'department_id'),
            ),
        }),
    )


    def is_answered(self, obj):
        return True if obj.answer_status == 'a' else False
    is_answered.boolean = True
