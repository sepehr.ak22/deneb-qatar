from django.contrib import admin

from  ticket.models import Comments
from .general import GeneralAdminDashboardDesign


@admin.register(Comments)
class CommentsAdmin(GeneralAdminDashboardDesign):
    list_filter = ('created', 'ticket_id', 'staff_id')
    list_display = ('staff_id', 'ticket_id', 'parent')
    list_display_links = ('staff_id', 'ticket_id')
    raw_id_fields = ('ticket_id', 'parent', 'staff_id')
