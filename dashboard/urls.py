from django.urls import path, re_path, include
from . import views
from ticket.views import TicketCreateView
from ticket.views import TicketDetailView
from project.views import ProjectView
from project.views import ProjectTemplateView
from project.views import ProjectFinalizeView
from project.views import ProjectShownView

from django.views.decorators.cache import cache_page


app_name = 'dashboard'
urlpatterns = [
    re_path(r'^$', views.HomeView.as_view(), name='home'),
    re_path(r'^account/$', views.AccountView.as_view(), name='account'),
    re_path(r'^account/team/list/$', views.TeamView.as_view(), name='project-team-list'),
    
    re_path(r'^account/profile/(?P<sku_app>\w+\d*)/$', views.AccountProfileUpdateView.as_view(), name='account-profile-edit'),
    re_path(r'^account/profile/(?P<sku_app>\w+\d*)/published/$', views.AccountProfileFinalizeView.as_view(), name='account-profile-published'),
    # cached
    re_path(r'^account/applications/$', views.ApplicationListView.as_view(), name='application-list'),

    re_path(r'^account/application/(?P<pk>\d+)/create/$', views.ApplicationCreateView.as_view(), name='application-create'),
    re_path(r'^account/application/(?P<sku>\w+\d*)/published/$', views.ApplicationFinalizeView.as_view(), name='application-published'),
    re_path(r'^account/application/(?P<sku>\w+\d*)/$', views.ApplicationView.as_view(), name='application-edit'),
    re_path(r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/$', ProjectView.as_view(), name='application-project-edit'),
    re_path(r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/shown/$', ProjectShownView.as_view(), name='application-project-shown'),
    re_path(r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/preview/$', ProjectTemplateView.as_view(), name='project-view'),
    re_path(r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/published/$', ProjectFinalizeView.as_view(), name='application-project-published'),
    re_path(r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/team/members/$', views.TeamMemberView.as_view(), name = 'project-team-member'),
    re_path(r'^account/application/(?P<sku>\w+\d*)/project/(?P<sku_project>\w+\d*)/team/published/$', views.TeamPublishedView.as_view(), name = 'project-team-published'),
    # cached
    re_path(r'^knowledge-base/$', cache_page(60*15)(views.KnowledgeBaseView.as_view()), name='knowledge-base'),
    re_path(r'^privacy-policy/$', cache_page(60*15)(views.PrivacyPolicyView.as_view()), name='privacy-policy'),
    re_path(r'^faq/$', cache_page(60*15)(views.FAQView.as_view()), name='faq'),
    
    re_path(r'^ticket/$', TicketCreateView.as_view(), name='ticket-create'),
    re_path(r'^ticket/(?P<sku>\w+)/$', TicketDetailView.as_view(), name='ticket-detail'),  
]