from django.views.generic import (
    View,
    TemplateView
)

from django.http import Http404

from django.contrib.auth.mixins import LoginRequiredMixin

from django.contrib import messages

from django.shortcuts import (
    redirect, 
    render,
    get_object_or_404,
)

from accounts.models import (
    Application,
    Profile
)
from django.http import JsonResponse
from django.http import HttpResponseNotFound

from django.urls import reverse_lazy

from django.forms.models import model_to_dict

from django.contrib.auth.models import User

from accounts.models import Profile
from accounts.models import Application
from accounts.forms import ProfileForm
from accounts.forms import ProfilePublishForm

from project.models import Project
from pages.models import Site

class AccountView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/pages/account.html"
    page_name = "Account"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        apps = Application.objects.filter(user = self.request.user)
        context['shown_project'] = Project.objects.filter(application__in = apps, is_shown = True)
        context['site'] = Site.objects.first()
        return context


class AccountProfileFinalizeView(LoginRequiredMixin, View):

    view = {
        'page_name': "Profile"
    }
    form = ProfilePublishForm

    def is_user_valid(self, request, user, *args, **kwargs):
        return True if request.user.pk == user.pk else False

    def is_published_valid(self, request, instance):
        if request.POST.get('is_published') == 'true':

            if  instance.user and instance.id_card and \
                instance.phone_number and instance.cover and instance.picture and \
                instance.origin and instance.residence and \
                instance.gender and instance.bio and instance.location and \
                (instance.instagram or instance.twitter or instance.facebook):

                messages.info(request, 'Profile\'s detail has been changed')

                # if all fields are set
                return True

            else:

                messages.warning(request, 'Profile\'s fields must be filled')

                # if one of field are none
                return False

        else:

            # if is_published set to false
            return False

    # def get(self, request, *args, **kwargs):
    #     return redirect("dashboard:home")

    def post(self, request, sku_app=None, *args, **kwargs):
        
        app = get_object_or_404(Application, sku = sku_app)
        user = app.user

        profile = Profile.objects.get(user = user)

        form = self.form(request.POST, instance=profile)

        if self.is_user_valid(request, user):

            if self.is_published_valid(request, profile):

                if form.is_valid():

                    messages.success(request, "Your profile is finalized.")
                    profile.is_published = True
                    profile.save()

                else:

                    for f, m in form.errors.get_json_data().items():

                        message = m[0]['message']

                        field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   
                        
                        messages.error(request, '{}: {}'.format(field, message))
            else:

                messages.error(request, "Your profile form is not valid.")
        else:

            raise Http404


        return redirect("dashboard:application-edit", sku_app) 


class AccountProfileUpdateView(LoginRequiredMixin, View):

    template_name = 'dashboard/pages/account-application-edit.html'
    view = {
        'page_name': "Profile"
    }
    form = ProfileForm


    def validate_phone_number(self, value):
        
        import re
        from django.core.exceptions import ValidationError
        from django.utils.translation import gettext_lazy as _

        phone_number_pattern = re.compile(r'^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$')
	
        if not bool(phone_number_pattern.match(value)):

            return False

        return True


    def check_clean_checkbox(self, request):

        return lambda file_filed: bool(request.FILES.get('{}-clean'.format(file_filed)))


    def check_file_field(self, request):

        return lambda file_filed: bool(request.FILES.get(file_filed))


    def file_filed_status(self, request, instance, value):

        check_clean_checkbox = self.check_clean_checkbox(request)
        check_file_field = self.check_file_field(request)
        
        if check_clean_checkbox(value) and not check_file_field(value):

            return None

        if not check_clean_checkbox(value) and check_file_field(value):

            return request.FILES.get(value)

        if not check_clean_checkbox(value) and not check_file_field(value):

            return getattr(instance, value)


    def is_user_valid(self, request, user, *args, **kwargs):

        return True if request.user.pk == user.pk else False


    # def get(self, request, pk=None, *args, **kwargs):
        
    #     user = get_object_or_404(User, pk = pk)
        
    #     if self.is_user_valid(request, user):
        
    #         form = self.form(initial=model_to_dict(user.profile))

    #         return render(request, self.template_name, {"form": form, "view": self.view })
        
    #     else:

    #         # messages.error(request, "Not valid request.")

    #         # return redirect('dashboard:account')
            
    #         return HttpResponseNotFound('<h1>Page not found</h1>')

    def post(self, request, sku_app=None, *args, **kwargs):
        
        app = get_object_or_404(Application, sku = sku_app)
        user = app.user
        
        profile = Profile.objects.get(user = user)

        form = self.form(request.POST, request.FILES, instance=profile)
        if self.is_user_valid(request, user):
            if form.is_valid():

                if self.validate_phone_number(request.POST.get('phone_number')):
            
                    profile.cover = self.file_filed_status(request, profile, 'cover')

                    profile.id_card = self.file_filed_status(request, profile, 'id_card')

                    profile.picture = self.file_filed_status(request, profile, 'picture')
                    
                    profile.phone_number = request.POST.get('phone_number')

                    profile.origin = request.POST.get('origin')

                    profile.gender = request.POST.get('gender')

                    profile.residence = request.POST.get('residence')

                    profile.bio = request.POST.get('bio')

                    profile.location = request.POST.get('location')

                    profile.instagram = request.POST.get('instagram')

                    profile.facebook = request.POST.get('facebook')

                    profile.twitter = request.POST.get('twitter')

                    profile.save()

                    messages.success(request, "Your profile draft has been changed.")

                    return redirect(reverse_lazy('dashboard:application-edit', kwargs={"sku":sku_app}), context = {"form": form, "view": self.view })
                    
                else:

                    messages.error(request, 'Please Enter valid Phone Number')

                    return redirect('dashboard:application-edit', sku_app)
            
            else:

                for f, m in form.errors.get_json_data().items():

                    message = m[0]['message']

                    field = 'Qid or Passport' if f == 'id_card' else ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   

                    messages.error(request, '{}: {}'.format(field, message))

                return redirect(reverse_lazy('dashboard:application-edit', kwargs={"sku":sku_app}), context = {"form": form, "view": self.view })
        
        else:

            
            # messages.error(request, "Not valid request.")
            return HttpResponseNotFound('<h1>Page not found</h1>')

        # return redirect(reverse_lazy('dashboard:account-profile-edit', kwargs={"pk":user.pk}), context = {"form": form, "view": self.view })
