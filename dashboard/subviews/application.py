from django.views.generic import (
    View,
    ListView,
    TemplateView
)

from django.contrib.auth.mixins import LoginRequiredMixin

from django.shortcuts import (
    redirect,
    render,
    get_object_or_404,
)

from django.forms.models import model_to_dict

from django.contrib import messages

from step.models import Step

from accounts.models import (
    Application,
    Team
)
from accounts.forms import (
    ApplicationForm, 
    ApplicationPublishForm,
    ProfileForm
)

from project.models import Project
from project.forms import ProjectForm

from pages.models import Site

class ApplicationListView(LoginRequiredMixin, ListView):
    model = Application
    context_object_name = 'applications'
    template_name = 'dashboard/pages/application-list.html'
    page_name = 'Application'

    def get_context_data(self, **kwargs):
        context = super(ApplicationListView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        return context


class ApplicationCreateView(LoginRequiredMixin, View):
    
    def recaptcha_validation(self):
        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': self.request.POST.get('g-recaptcha-response'),
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False
    
    
    def is_profile_published(self):

        return True if self.request.user.profile.is_published else False
    

    def post(self, request, pk=None, *args, **kwargs):
        
        if self.is_profile_published():

            if self.recaptcha_validation():

                app = Application.objects.create(user = request.user)
    
                return redirect('dashboard:application-edit', app.sku)
            
            else:

                messages.error(request, 'Captcha Failure')

        else:

            messages.error(request, 'You must first finalize your profile')

        return redirect('dashboard:application-list')


class ApplicationFinalizeView(LoginRequiredMixin, View):

    view = {
        'page_name': "Application"
    }
    form = ApplicationPublishForm


    def recaptcha_validation(self):
        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': self.request.POST.get('g-recaptcha-response'),
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False

    
    def validate_kafil_table(self, instance):

        if instance.qatari_partnership:

            kafil_fname = instance.kafil_fname
            kafil_lname = instance.kafil_lname
            kafil_email = instance.kafil_email
            kafil_qatari_id = instance.kafil_qatari_id

            if not (kafil_fname and kafil_lname and kafil_email and kafil_qatari_id):

                messages.error(self.request, "You must fill kafil information")

                return False

            else:

                return True
        
        else:

            kafil_fname = instance.kafil_fname
            kafil_lname = instance.kafil_lname
            kafil_email = instance.kafil_email
            kafil_qatari_id = instance.kafil_qatari_id

            if kafil_fname or kafil_lname or kafil_email or kafil_qatari_id:

                messages.error(self.request, "You must select True for Qatari Partnership")

                return False

            else:

                return True


    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.user else False


    def is_published_valid(self, request, instance):

        if request.POST.get('is_published') == 'true':
            
            if  instance.user and \
                instance.website and instance.problem and \
                instance.solution and instance.deneb_result and \
                instance.industry and \
                (instance.is_b2b or instance.is_b2c or \
                instance.is_b2g or instance.is_b2b2c):
                
                
                messages.info(request, 'Application\'s detail has been changed')

                # if all fields are set
                return True

            else:

                messages.warning(request, 'Application\'s fields must be filled')

                # if one of field are none
                return False

        else:

            # if is_published set to false
            return False
        
    def is_profile_published(self):

        return True if self.request.user.profile.is_published else False

    def get(self, request, sku, *args, **kwargs):
        return redirect("dashboard:home")

    def post(self, request, sku, *args, **kwargs):
        # import pdb ; pdb.set_trace()
        if self.recaptcha_validation():

            if self.is_profile_published():

                application = get_object_or_404(Application, sku = sku)

                form = self.form(request.POST, instance=application)

                if self.is_user_valid(request, application):

                    if self.is_published_valid(request, application):
                    
                        if form.is_valid():

                            if self.validate_kafil_table(application):

                                messages.success(request, "Your application is finalized.")

                                application.is_published = True
                                application.save()

                                return redirect("dashboard:application-edit", sku)
                                
                            else:

                                messages.error(request, "Your application form is not complete.")

                        else:

                            for f, m in form.errors.get_json_data().items():

                                message = m[0]['message']

                                field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   
                                
                                messages.error(request, '{}: {}'.format(field, message))

                else:
                    messages.error(request, "Not valid request.")
            else:
                messages.error(request, "You must finalize your profile.")
        else:
            messages.error(request, "Captch Failure.")

        return redirect("dashboard:application-edit", sku)


class ApplicationView(LoginRequiredMixin, View):

    template_name = 'dashboard/pages/application-process.html'
    view = {
        'page_name': "Application"
    }
    form = ApplicationForm


    def validate_kafil_table(self):

        if self.checkbox_status(self.request, 'qatari_partnership'):

            kafil_fname = self.request.POST.get('kafil_fname')
            kafil_lname = self.request.POST.get('kafil_lname')
            kafil_email = self.request.POST.get('kafil_email')
            kafil_qatari_id = self.request.POST.get('kafil_qatari_id')


            if not (kafil_fname and kafil_lname and kafil_email and kafil_qatari_id):

                messages.error(self.request, "You must fill kafil information")

                return False

            else:

                return True
        
        else:

            kafil_fname = self.request.POST.get('kafil_fname')
            kafil_lname = self.request.POST.get('kafil_lname')
            kafil_email = self.request.POST.get('kafil_email')
            kafil_qatari_id = self.request.POST.get('kafil_qatari_id')

            if kafil_fname or kafil_lname or kafil_email or kafil_qatari_id:

                messages.error(self.request, "You must select True for Qatari Partnership")

                return False

            else:

                return True

    
    def radio_button_status(self, request, value):
        
        if request.POST.get(value) == 'on':

            return True

        else:

            return False

    def checkbox_status(self, request, value):
        
        if request.POST.get(value) == 'True':

            return True

        else:

            return False
    
    
    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.user else False
    
    def find_project(self, request, instance, *args, **kwargs):
        
        try:
            project = get_object_or_404(Project, sku=instance.project.sku)

            if self.is_user_valid(request, instance):

                form = ProjectForm(initial=model_to_dict(project))
            else:
                form = None
        except:
            project = None
            form = None

        return project, form

    def get(self, request, sku=None, *args, **kwargs):

        app = get_object_or_404(Application, sku = sku)
        last_step = Step.objects.filter(application = app).filter(state='c').last()
        if last_step:
            current_step = 5 if int(last_step.step) == 6 else int(last_step.step)
        else:
            if app.user.profile.is_published:
                current_step = 1
            else:
                current_step = 0
        
        # print(current_step)
        project, project_form = self.find_project(request, app)
        team = Team.objects.filter(project = project)
        profile_form = ProfileForm(initial=model_to_dict(request.user.profile))

        if team.exists():
            team = team[0]
        else:
            team = None

        if self.is_user_valid(request, app):

            form = self.form(initial=model_to_dict(app))
            
            return render(request, self.template_name, {
                "form": form, 
                "view": self.view, 
                "application": app, 
                'current_step': current_step,
                'project': project,
                'project_form': project_form,
                'team': team,
                'profile_form': profile_form,
                'site': Site.objects.first()
            })
        
        message = messages.error(request, "Not valid request.")
        

        return redirect('dashboard:application-list', {'message':message, 'current_step': current_step})

    def post(self, request, sku=None, *args, **kwargs):

        app = get_object_or_404(Application, sku = sku)

        form = self.form(request.POST, instance=app)

        if self.is_user_valid(request, app):

            if form.is_valid():

                if self.validate_kafil_table():
                    # import pdb ; pdb.set_trace()
                    form.save(commit=False)

                    app.qatari_partnership = self.checkbox_status(request, 'qatari_partnership')

                    app.website = request.POST.get('website')

                    app.problem = request.POST.get('problem')

                    app.solution = request.POST.get('solution')

                    app.industry = '' if not request.POST.get('industry') else request.POST.get('industry')

                    app.deneb_result = '' if not request.POST.get('deneb_result') else request.POST.get('deneb_result')

                    app.is_b2b = self.radio_button_status(request, 'is_b2b')

                    app.is_b2c = self.radio_button_status(request, 'is_b2c')

                    app.is_b2g = self.radio_button_status(request, 'is_b2g')

                    app.is_b2b2c = self.radio_button_status(request, 'is_b2b2c')

                    app.has_product = self.checkbox_status(request, 'has_product')

                    app.sales_rate = 0 if not request.POST.get('sales_rate') else int(request.POST.get('sales_rate'))

                    app.sales_rate_type = request.POST.get('sales_rate_type')

                    app.kafil_fname = self.request.POST.get('kafil_fname')
                    
                    app.kafil_lname = self.request.POST.get('kafil_lname')
                    
                    app.kafil_email = self.request.POST.get('kafil_email')
                    
                    app.kafil_qatari_id = self.request.POST.get('kafil_qatari_id')

                    app.save()
                    
                    messages.success(request, "Your application has been changed.")

                    return redirect('dashboard:application-edit', app.sku)

                else:

                    return redirect('dashboard:application-edit', app.sku)

            else:
                
                for f, m in form.errors.get_json_data().items():

                    message = m[0]['message']

                    field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   
                    
                    messages.error(request, '{}: {}'.format(field, message))

                return redirect('dashboard:application-edit', app.sku)
        
        else:

            messages.error(request, "Not valid request.")

            
            return redirect('dashboard:application-edit', app.sku)