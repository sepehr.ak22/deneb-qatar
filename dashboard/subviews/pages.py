from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

from pages.models import Site

class KnowledgeBaseView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/pages/knowledge-base.html"
    page_name = "Knowledge Base"

    def get_context_data(self, **kwargs):
        context = super(KnowledgeBaseView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        return context


class FAQView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/pages/faq.html"
    page_name = "FAQ"

    def get_context_data(self, **kwargs):
        context = super(FAQView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        return context


class PrivacyPolicyView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/pages/privacy-policy.html"
    page_name = "PrivacyPolicy"
    
    def get_context_data(self, **kwargs):
        context = super(PrivacyPolicyView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        return context


class ResponsiblityView(LoginRequiredMixin, TemplateView):

    template_name = ""
    page_name = "Responsiblity"

    def get_context_data(self, **kwargs):
        context = super(ResponsiblityView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        return context


class MemberView(LoginRequiredMixin, TemplateView):

    template_name = ""
    page_name = "Member"

    def get_context_data(self, **kwargs):
        context = super(MemberView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        return context


class TeamView(LoginRequiredMixin, TemplateView):

    template_name = "dashboard/pages/project-team-list.html"
    page_name = "Team"

    def get_context_data(self, **kwargs):
        context = super(TeamView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        return context


class ExperienceView(LoginRequiredMixin, TemplateView):

    template_name = ""
    page_name = "Experience"

    def get_context_data(self, **kwargs):
        context = super(ExperienceView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        return context