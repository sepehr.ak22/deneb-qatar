from django.views.generic import View

from django.contrib import messages

from django.contrib.auth.mixins import LoginRequiredMixin

from django.shortcuts import (
    redirect,
    render,
    get_object_or_404,
)

from accounts.models import (
    Team,
    Member
)

from accounts.forms import (
    TeamForm, 
    MemberForm,
    TeamPublishedForm
)

from project.models import Project


class TeamView(LoginRequiredMixin, View):

    view = {
        'page_name': "Team"
    }
    form = TeamForm 


    def recaptcha_validation(self):
        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': self.request.POST.get('g-recaptcha-response'),
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False


    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.user else False


    def post(self, request, sku_project, *args, **kwargs):
        
        project = get_object_or_404(Project, sku = sku_project)

        team = project.team
        form = self.form(request.POST, instance=team)
        if self.recaptcha_validation():
        
            if self.is_user_valid(request, project):

                if form.is_valid():

                    team.user = request.user

                    team.name = request.POST.get('name')

                    team.project = project

                else:
                    
                    messages.error(request, "Form is not valid")

                    return redirect('dashboard:application-edit', project.application.sku)

            else:
                
                messages.error(request, "Not valid request")
        
        else:
            
            messages.error(request, "Not valid request")

        return redirect('dashboard:application-edit', project.application.sku)


class TeamMemberView(LoginRequiredMixin, View):

    view = {
        'page_name': "TeamMember"
    }
    form = MemberForm


    def validate_phone_number(self, value):
        
        import re
        from django.core.exceptions import ValidationError
        from django.utils.translation import gettext_lazy as _

        phone_number_pattern = re.compile(r'^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$')
	
        if not bool(phone_number_pattern.match(value)):

            return False

        return True


    def validate_email(self, value):
        
        import re
        from django.core.exceptions import ValidationError

        email_pattern = re.compile(r'^\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b')
        
        if not bool(email_pattern.match(value)):

            return False
        
        return True


    def is_valid(self):
        
        try:
            
            if not self.request.POST.get('team-name'):
                
                messages.error(self.request, "You must fill team name and at least one member.")

                return False

            fnames = self.request.POST.getlist("item_fname")

            lnames = self.request.POST.getlist("item_lname")

            positions = self.request.POST.getlist("item_position")

            phone_numbers = self.request.POST.getlist("item_phone")

            emails = self.request.POST.getlist("item_email")

            if len(fnames) == 0 or len(lnames) == 0 or \
               len(positions) == 0 or len(phone_numbers) == 0 or \
               len(emails) == 0: 

               messages.error(self.request, "You must fill team name and at least one member.")

               return False

            for fname, lname, position, phone_number, email in zip(fnames, lnames, positions, phone_numbers, emails):

                if not (fname and lname and position and phone_number and email): 
                    
                    messages.error(self.request, "You must fill team name and at least one member.")

                    return False

            flag = True

            for email in emails:

                if not self.validate_email(email):

                    messages.error(self.request, '{} is not a valid email'.format(email))
                    
                    flag = False

            for phone_number in phone_numbers:

                if not self.validate_phone_number(phone_number):

                    messages.error (self.request, '{} is not a valid phone number'.format(phone_number))

                    flag = False

            return True if flag else False
            
        except :

            return False
    
    
    def recaptcha_validation(self):

        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': self.request.POST.get('g-recaptcha-response'),
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False


    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.application.user else False
    
    
    def get(self, request, sku_project, *args, **kwargs):
        pass


    def post(self, request, sku_project, *args, **kwargs):
        
        
        project = get_object_or_404(Project, sku = sku_project)
       
        if self.is_user_valid(request, project):
            
            if self.is_valid():
               
                team_name = request.POST.get('team-name')

                try:

                    if project.team:

                        team = project.team
                        
                        if project.team.name != team_name:
                            
                            team.name = team_name
                            team.save()
                            messages.success(request, "Your team name changed")
                        

                except:

                    team, _ = Team.objects.get_or_create(name = team_name, user = request.user, project = project)
                
                fnames = request.POST.getlist("item_fname")

                lnames = request.POST.getlist("item_lname")

                positions = request.POST.getlist("item_position")

                phone_numbers = request.POST.getlist("item_phone")

                emails = request.POST.getlist("item_email")

                for fname, lname, position, phone_number, email in zip(fnames, lnames, positions, phone_numbers, emails):

                    Member.objects.create(
                        first_name = fname,
                        last_name = lname,
                        position = position,
                        phone_number = phone_number,
                        email = email,
                        team = team
                    )

                messages.success(request, "Your members team added")

            else:
                
                return redirect('dashboard:application-edit', project.application.sku)

        else:
            
            messages.error(request, "Not valid request")
        
        return redirect('dashboard:application-edit', project.application.sku)


class TeamPublishedView(LoginRequiredMixin, View):

    view = {
        'page_name': "Team"
    }
    form = TeamPublishedForm


    def recaptcha_validation(self):
        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': self.request.POST.get('g-recaptcha-response'),
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False

    
    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.project.application.user else False


    def is_published_valid(self, request, instance):

        if request.POST.get('is_published') == 'true':
            
            if  instance.user and \
                instance.project and instance.name:

                try:

                    team_members = instance.colleagues.all()

                    if team_members:

                        # if all fields are set
                        return True

                except:

                    return False
                
            else:

                messages.warning(request, 'You must fill team name and at least one member.')

                # if one of field are none
                return False

        else:

            # if is_published set to false
            return False
    

    def is_project_published(self, project):

        return True if project.is_published else False


    def post(self, request, sku_project, *args, **kwargs):

        project = get_object_or_404(Project, sku = sku_project)

        if self.recaptcha_validation():

            if self.is_project_published(project):
                
                try:
                    team = Team.objects.get(project = project)
                except:
                    messages.error(request, "Please Insert your team information.")
                    return redirect('dashboard:application-edit', project.application.sku)

                form = self.form(request.POST, instance=team)

                if self.is_user_valid(request, team):

                    if self.is_published_valid(request, team):
                        if form.is_valid():

                            messages.success(request, "Your team is finalized.")

                            team.is_published = True
                            team.save()

                            return redirect('dashboard:application-edit', project.application.sku)

                        else:

                            messages.error(request, "Your team form is not complete.")
                    else:
                        messages.error(request, "Not valid request.")
                else:
                    messages.error(request, "Not valid request.")
            else:
                messages.error(request, "You must finalize your project.")
        else:
            messages.error(request, "Captcha Failure.")

        return redirect('dashboard:application-edit', project.application.sku)