from django.contrib.auth.models import User
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.cache import cache

from pages.models import Site

from accounts.models import (
    Application,
    Profile
)
from project.models import Project
from ticket.models import Ticket
from step.models import Step

from dashboard.subviews.application import (
    ApplicationListView,
    ApplicationCreateView,
    ApplicationFinalizeView,
    ApplicationView,
)

from dashboard.subviews.pages import(
    KnowledgeBaseView,
    FAQView,
    PrivacyPolicyView,
    ResponsiblityView,
    MemberView,
    TeamView,
    ExperienceView,
)

from dashboard.subviews.profile import (
    AccountView,
    AccountProfileFinalizeView,
    AccountProfileUpdateView,
)

from dashboard.subviews.team import(
    TeamMemberView,
    TeamPublishedView
)

class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/pages/home.html"
    page_name = 'Dashboard'


    def get_application_status(self, app):

        steps = Step.objects.filter(application = app)
        # import pdb ; pdb.set_trace()
        count = Step.objects.filter(application = app, state = 't').count()

        if count == 5:

            return False

        for step in steps:

            if step.state == 'i' : return False

        return True


    def get_profile_status(self):

        step = Step.objects.filter(user = self.request.user, step = '1', state = 'i')

        return False if step else True


    def get_requests_status_objects(self, type):

        apps = Application.objects.filter(user = self.request.user)

        step = Step.objects.filter(user = self.request.user, step = '1', state = 'i')

        if type == 'inprogress':

            inprogress = list()
            
            inprogress_count = 0

            for app in apps:

                if not self.get_application_status(app):
                    
                    inprogress_count += 1

                    inprogress.append(app)

            if step:

                inprogress_count += 1
            
            return dict([('count', inprogress_count), ('inprogress', inprogress)])
        
        if type == 'complete':

            complete = list()

            complete_count = 0

            for app in apps:

                if self.get_application_status(app):
                    
                    complete_count += 1

                    complete.append(app)

            if not step:

                complete_count += 1

            return dict([('count', complete_count), ('complete', complete)])


    def get_number_of_complete_step_application(self):

        apps = Application.objects.filter(user = self.request.user)

        count = Application.objects.filter(user = self.request.user).count()
        
        complete = 0

        for app in apps:
            
            step = Step.objects.filter(application = app, state = 'c', step = '2').exists()


            if step:
                complete = complete + 1
        
        
        result = (complete / count) * 100
        return result, complete, count 


    def get_number_of_complete_step_project(self):

        apps = Application.objects.filter(user = self.request.user)

        count = Project.objects.filter(application__in = apps).count()
        complete = 0
        for app in apps:

            project = Project.objects.filter(application = app)

            if project:

                step = Step.objects.filter(application = app, state = 'c', step = '3')

                if step: complete += 1

            else:

                continue
        
        try:
            result = (complete / count) * 100
        except:
            result = 0

        return result, complete, count 


    def project_status(self, project):

        project_progress_num = 15

        attrs = vars(project)

        for key in attrs.keys():
            if key in ['is_published',
                       'is_shown',
                       'title',
                       'summary',
                       'problem',
                       'solution',
                       'product',
                       'customer',
                       'business',
                       'market',
                       'competition',
                       'vision',
                       'founders',
                       'cover', 'video']:
                if not attrs[key]:
                    project_progress_num -= 1
            else:
                continue

        percent_complete_of_project = (project_progress_num / 15) * 100
      
        return (attrs['sku'], percent_complete_of_project)


    def application_status(self, app):

        attrs = vars(app)

        extra_field = ['_state', 'id', 'sku', 'user_id',
                       'created', 'modified', 'webiste',
                       'is_b2b', 'is_b2c', 'is_b2g', 'is_b2b2c',
                       'has_product', 'qatari_partnership', 'sales_rate', 'sales_rate_type']

        if attrs['qatari_partnership']:

            application_progress_num = 11

        else:

            application_progress_num = 7

            extra_field.extend(['kafil_fname', 'kafil_lname', 'kafil_email', 'kafil_qatari_id'])

        total = application_progress_num

        if not (attrs['is_b2b'] or attrs['is_b2c'] or attrs['is_b2g'] or attrs['is_b2b2c']):

            application_progress_num -= 1

        for key in attrs.keys():
            if key in extra_field:
                continue
            else:
                if not attrs[key]:
                    application_progress_num -= 1

        percent_complete_of_application = (application_progress_num / total) * 100

        return (attrs['sku'], percent_complete_of_application)
    

    def whole_process(self, app):
        result = (Step.objects.filter(application = app, state = 'c').count() / 5) * 100
        return app.sku, result
    
    
    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)

        profile = Profile.objects.get(user = self.request.user)
        
        profile_progress_num = 13
        attrs = vars(profile)
        for key, value in attrs.items():
            if key in ['_state', 'id', 'user_id', 'width_field', 'height_field', 'width_field_pic', 'height_field_pic', 'email_confirmed', 'modified', 'is_shown']:
                continue
            else:
                if not attrs[key]:
                    profile_progress_num -= 1
       
        
        context['profile_progress'] = (profile_progress_num / 13) * 100
        context['profile_progress_num'] = profile_progress_num
        context['profile_status'] = self.get_profile_status() # if Flase: profile is inprogress, if True: profile is complete

        context['site'] = Site.objects.first()
    
        context["ticket_count"] = Ticket.objects.filter(raised_by = self.request.user.pk).count()
        context["tickets"] = Ticket.objects.filter(raised_by = self.request.user.pk)

        context["answered_ticket_count"] = Ticket.objects.filter(raised_by = self.request.user.pk,
                                                                 answer_status = 'a'
                                                                ).count()

        context["unanswered_ticket_count"] = Ticket.objects.filter(raised_by = self.request.user.pk,
                                                                 answer_status = 'u'
                                                                ).count()

        apps = Application.objects.filter(user = self.request.user)

        context["project_count"] = Project.objects.filter(application__in = apps).count()

        context["projects"] = Project.objects.filter(application__in = apps)
        context["apps"] = apps

        inprogress = self.get_requests_status_objects('inprogress')
        context["inprogress_requests_count"] = inprogress['count'] # number of inprogress requests
        context["inprogress_requests"] = inprogress['inprogress']  # list of inprogress requests

        complete = self.get_requests_status_objects('complete')
        context["complete_requests_count"] = complete['count'] # number of complete requests
        context["complete_requests"] = complete['complete'] # list of complete requests

        # import pdb ; pdb.set_trace()
        complete_applications_progress, complete_applications_current, complete_applications_total  = self.get_number_of_complete_step_application()
        context['complete_applications_progress'] = complete_applications_progress
        context['complete_applications_total'] = complete_applications_total
        context['complete_applications_current'] = complete_applications_current

        complete_project_progress, complete_project_current, complete_project_total  = self.get_number_of_complete_step_project()
        context['complete_project_progress'] = complete_project_progress
        context['complete_project_total'] = complete_project_total
        context['complete_project_current'] =  complete_project_current


        context['applications_status'] = list(map(lambda app: self.application_status(app), apps))
        # import pdb ; pdb.set_trace()
        projects = Project.objects.filter(application__in = apps)
        
        context['projects_status'] = list(map(lambda app: self.project_status(app), projects))
        
        context['whole_process'] = list(map(lambda app: self.whole_process(app), apps))
        
        return context

# Modularity Views checked
# Team Form UI
# Team Form Backend
# Team database submission
# Profile ?

# Test Project hich moshkeli nadashte