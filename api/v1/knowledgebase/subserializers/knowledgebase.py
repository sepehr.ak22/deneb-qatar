from rest_framework import serializers

from knowledgebase.models import KnowledgeBase


class KnowledgeBaseSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:

        model = KnowledgeBase

        fields = ('slug', 'title', 'category', 'description', 'priority')