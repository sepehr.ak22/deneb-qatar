from rest_framework import serializers

from knowledgebase.models import KnowledgeBaseCategory


class KnowledgeBaseCategorySerializer(serializers.HyperlinkedModelSerializer):

    
    class Meta:

        model = KnowledgeBaseCategory

        fields = ('slug', 'title', 'priority')