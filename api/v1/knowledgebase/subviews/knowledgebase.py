from django.db.models.query import QuerySet

from rest_framework.viewsets import ModelViewSet

from rest_framework.decorators import action

from rest_framework.response import Response

from django_filters.rest_framework import DjangoFilterBackend

from knowledgebase.models import KnowledgeBase

from api.v1.knowledgebase.serializers import KnowledgeBaseSerializer

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)

from rest_framework.permissions import (
    IsAuthenticated,
    DjangoModelPermissions
)


class KnowledgeBaseViewSet(ModelViewSet):

    queryset = KnowledgeBase.objects.all()
    
    serializer_class = KnowledgeBaseSerializer

    permission_classes = [
        IsAuthenticated,
        DjangoModelPermissions
    ]

    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_fields = ['category', 'priority']
    ordering_fields = ['created',]
    search_fields = ['title',]

    
    @action(detail = False, methods = ['get'])
    def newest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').last()
        serializer = self.get_serializer_class()(newest, context={'request': request})
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def oldest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').first()
        serializer = self.get_serializer_class()(newest, context={'request': request})
        return Response(serializer.data)