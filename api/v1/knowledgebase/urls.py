from rest_framework import routers

from . import views as view


app_name= 'api_v1_knowledgebase'

router = routers.SimpleRouter()

router.register(r'knowledges_base', view.KnowledgeBaseViewSet)
router.register(r'knowledge_base_categories', view.KnowledgeBaseCategoryViewSet)

urlpatterns = router.urls