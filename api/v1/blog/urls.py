from rest_framework import routers

from . import views as view


app_name= 'api_v1_blog'

router = routers.SimpleRouter()

router.register(r'posts', view.PostViewSet)
router.register(r'post_categories', view.CategoryViewSet)

urlpatterns = router.urls