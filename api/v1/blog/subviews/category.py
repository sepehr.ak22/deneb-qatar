from django.db.models.query import QuerySet

from rest_framework.viewsets import ModelViewSet

from rest_framework.decorators import action

from rest_framework.response import Response

from django_filters.rest_framework import DjangoFilterBackend

from blog.models import Category

from api.v1.blog.serializers import CategorySerializer

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)

from rest_framework.permissions import (
    IsAuthenticated,
    DjangoModelPermissions
)

from api.v1.blog.serializers import PostSerializer


class CategoryViewSet(ModelViewSet):

    queryset = Category.objects.all()
    
    serializer_class = CategorySerializer

    permission_classes = [
        IsAuthenticated,
        DjangoModelPermissions
    ]

    filter_backends = [SearchFilter, OrderingFilter]
    ordering_fields = ['created',]
    search_fields = ['title',]

    
    @action(detail=True, methods = ['get'])
    def posts(self, request, pk = None):
        category = self.get_object()
        serializer = PostSerializer(category.articles.all(),
                                    many = True,
                                   )
        return Response(serializer.data)
    
    
    @action(detail = False, methods = ['get'])
    def newest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def oldest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').first()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)