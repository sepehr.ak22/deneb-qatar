from django.db.models.query import QuerySet

from rest_framework.viewsets import ModelViewSet

from rest_framework.decorators import action

from rest_framework.response import Response

from django_filters.rest_framework import DjangoFilterBackend

from accounts.models import Profile

from api.v1.accounts.serializers import ProfileSerializer

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)

from rest_framework.permissions import (
    IsAuthenticated,
    DjangoModelPermissions
)


class ProfileViewSet(ModelViewSet):

    lookup_field = 'sku'

    queryset = Profile.objects.all()
    
    serializer_class = ProfileSerializer

    permission_classes = [
        IsAuthenticated,
        DjangoModelPermissions
    ]

    filter_backends = [SearchFilter, OrderingFilter]
    ordering_fields = ['modified',]
    search_fields = ['user',]
    

    def get_queryset(self):

        assert self.queryset is not None, (
            "'%s' should either include a `queryset` attribute, "
            "or override the `get_queryset()` method."
            % self.__class__.__name__
        )

        queryset = self.queryset

        if isinstance(queryset, QuerySet):

            queryset = queryset.filter(user = self.request.user)
        
        return queryset 
    
    
    @action(detail = False, methods = ['get'])
    def newest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def oldest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').first()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)