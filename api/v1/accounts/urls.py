from rest_framework import routers

from . import views as view


app_name= 'api_v1_accounts'

router = routers.SimpleRouter()

router.register(r'profile', view.ProfileViewSet)
router.register(r'applications', view.ApplicationViewSet)
router.register(r'responsiblity', view.ResponsiblityViewSet)
router.register(r'teams', view.TeamViewSet)
router.register(r'members', view.MemberViewSet)
router.register(r'experiences', view.ExperienceViewSet)

urlpatterns = router.urls