from rest_framework import serializers

from accounts.models import Experience


class ExperienceSerializer(serializers.ModelSerializer):


    class Meta:

        model = Experience

        fields = ('user',
                  'title',
                  'company',
                  'location',
                  'from_date',
                  'to_date',
                  'currently_work',
                  'description', 
                 )








