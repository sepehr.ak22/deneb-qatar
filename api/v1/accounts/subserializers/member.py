from rest_framework import serializers

from accounts.models import Member


class MemberSerializer(serializers.ModelSerializer):


    class Meta:

        model = Member

        fields = ('first_name', 'last_name', 'team',
                  'position', 'picture'  
                 )