from rest_framework import serializers

from accounts.models import Team


class TeamSerializer(serializers.ModelSerializer):
    project = serializers.CharField(source='project.sku', read_only=True)
    
    class Meta:

        model = Team

        fields = ('name', 'project', 'user')