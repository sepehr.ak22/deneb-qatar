from rest_framework import serializers

from accounts.models import Responsiblity


class ResponsiblitySerializer(serializers.ModelSerializer):


    class Meta:

        model = Responsiblity

        fields = ('slug', 'title', 'user')