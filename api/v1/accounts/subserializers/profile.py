from rest_framework import serializers

from accounts.models import Profile

from django_countries.serializer_fields import CountryField


class ProfileSerializer(serializers.ModelSerializer):

    origin = CountryField()

    residence = CountryField()

    class Meta:

        model = Profile

        fields = ('user',
                  'id_card',
                  'phone_number',
                  'cover',
                  'picture',
                  'is_published',
                  'origin',
                  'residence',
                  'gender',
                  'bio',
                  'location',
                  'email_confirmed',
                  'instagram',
                  'facebook',
                  'twitter',
                 )


