from rest_framework import serializers

from accounts.models import Application
from project.models import Project


class ApplicationSerializer(serializers.ModelSerializer):
    project = serializers.CharField(source='project.sku', read_only=True)

    class Meta:

        model = Application

        fields = ('sku', 'user', 'qatari_partnership',
                  'website', 'problem', 'solution',
                  'is_b2b', 'is_b2c', 'is_b2g', 'is_b2b2c', 'industry', 'has_product',
                  'sales_rate', 'deneb_result', 'is_published', 'project'
                 )
    
   