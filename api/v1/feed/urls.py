from django.urls import re_path

from . import views as view


urlpatterns = [
    re_path(r'posts', view.PostFeed(), name = "post_feed"),
    re_path(r'projects', view.ProjectFeed(), name = "project_feed"),
]