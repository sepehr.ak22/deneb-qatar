from django.db.models.query import QuerySet

from rest_framework.viewsets import ModelViewSet

from rest_framework.decorators import action

from rest_framework.response import Response

from django_filters.rest_framework import DjangoFilterBackend

from project.models import Comment

from api.v1.project.serializers import CommentSerializer

from rest_framework.filters import OrderingFilter

from rest_framework.permissions import (
    IsAuthenticated,
    DjangoModelPermissions
)


class CommentViewSet(ModelViewSet):

    queryset = Comment.objects.all()
    
    serializer_class = CommentSerializer

    permission_classes = [
        IsAuthenticated,
        DjangoModelPermissions
    ]

    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filter_fields = ['is_shown', 'project']
    ordering_fields = ['created',]
    
    
    @action(detail = False, methods = ['get'])
    def newest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def oldest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').first()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)