from rest_framework.views import APIView

from rest_framework.response import Response

from project.models import Project

from api.v1.project.serializers import ProjectHitCountSerializer



class ProjectHitCountView(APIView):


    def get(self, request, format=None):

        applications_query = request.user.applications.all()

        projects = Project.objects.filter(application__in = applications_query, is_shown = True)

        serializer = ProjectHitCountSerializer(projects, many=True, context={'request': request})

        return Response(serializer.data)  