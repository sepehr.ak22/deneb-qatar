from django.db.models.query import QuerySet

from django.db.models import Q

from rest_framework.viewsets import ModelViewSet

from rest_framework.decorators import action

from rest_framework.response import Response

from django_filters.rest_framework import DjangoFilterBackend

from project.models import Project

from api.v1.project.serializers import ProjectSerializer


from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)

from rest_framework.permissions import (
    IsAuthenticated,
    DjangoModelPermissions
)

from painless.permissions import (
    AppViewPermission,
)


class ProjectViewSet(ModelViewSet):

    queryset = Project.objects.all()
    
    serializer_class = ProjectSerializer

    permission_classes = [
        IsAuthenticated,
        DjangoModelPermissions
    ]

    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_fields = ['application', 'is_shown', 'is_published']
    ordering_fields = ['created',]
    search_fields = ['title',]

    def permission_status(self, request, model_cls):
        
        return AppViewPermission().has_permission(request, model_cls)

    
    def get_projects_query(self, request):
        
        applications_query = request.user.applications.all()

        projects_query = Project.objects.filter(Q(application__in = applications_query) | \
                                                (~Q(application__in = applications_query) , 
                                                 Q(is_published=True),
                                                 Q(is_shown=True)
                                                ) 
                                               )

        return projects_query

    
    def get_queryset(self):

        assert self.queryset is not None, (
            "'%s' should either include a `queryset` attribute, "
            "or override the `get_queryset()` method."
            % self.__class__.__name__
        )

        if self.permission_status(self.request, self.queryset.model):

            queryset = self.queryset

            if isinstance(queryset, QuerySet):

                queryset = queryset.all()
        else:

            queryset = self.get_projects_query(self.request)

        return queryset 
    
    
    # @action(detail=False, methods = ['get'])
    # def counters(self, request, pk = None):
    #     projects = self.get_queryset().filter(raised_by = request.user)
    #     serializer = self.get_serializer_class()(tickets, many = True)
    #     return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def newest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def oldest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').first()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)