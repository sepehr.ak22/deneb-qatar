from rest_framework import serializers

from project.models import Comment


class CommentSerializer(serializers.ModelSerializer):

    
    class Meta:

        model = Comment

        fields = ('message', 'submited_by', 'is_shown', 'project')