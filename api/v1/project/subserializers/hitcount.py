from rest_framework import serializers

from django.core.serializers import serialize

from datetime import timedelta

from django.utils import timezone

from hitcount.models import Hit

from project.models import Project

from accounts.models import Application


class ProjectHitCountSerializer(serializers.Serializer):

    sku = serializers.CharField()
    title = serializers.CharField()
    application = serializers.PrimaryKeyRelatedField(queryset = Application.objects.all())
    user = serializers.PrimaryKeyRelatedField(source='application.user', read_only=True)
    hits_count = serializers.SerializerMethodField()
    hits_time = serializers.SerializerMethodField()
    

    def get_hits_count(self, obj):

        return obj.hit_count_generic.all().values_list('hits', flat=True)

    def get_hits_time(self, obj):

        hits = obj.hit_count_generic.all()

        toady_queryset = Hit.objects.filter(hitcount__in = hits, created__date = timezone.now())
        one_day_ago_queryset = Hit.objects.filter(hitcount__in = hits, created__date = timezone.now()-timedelta(days=1))
        two_days_ago_queryset = Hit.objects.filter(hitcount__in = hits, created__date = timezone.now()-timedelta(days=2))
        three_days_ago_queryset = Hit.objects.filter(hitcount__in = hits, created__date = timezone.now()-timedelta(days=3))
        four_days_ago_queryset = Hit.objects.filter(hitcount__in = hits, created__date = timezone.now()-timedelta(days=4))
        five_days_ago_queryset = Hit.objects.filter(hitcount__in = hits, created__date = timezone.now()-timedelta(days=5))
        six_days_ago_queryset = Hit.objects.filter(hitcount__in = hits, created__date = timezone.now()-timedelta(days=6))
        seven_days_ago_queryset = Hit.objects.filter(hitcount__in = hits, created__date = timezone.now()-timedelta(days=7))

        get_created_time = lambda query: query.created
        
        toady = list(map(get_created_time, toady_queryset)) if toady_queryset else list()

        one_day_ago = list(map(get_created_time, one_day_ago_queryset)) if one_day_ago_queryset else list()

        two_days_ago = list(map(get_created_time, two_days_ago_queryset)) if two_days_ago_queryset else list()

        three_days_ago = list(map(get_created_time, three_days_ago_queryset)) if three_days_ago_queryset else list()

        four_days_ago = list(map(get_created_time, four_days_ago_queryset)) if four_days_ago_queryset else list()

        five_days_ago = list(map(get_created_time, five_days_ago_queryset)) if five_days_ago_queryset else list()

        six_days_ago = list(map(get_created_time, six_days_ago_queryset)) if six_days_ago_queryset else list()
        
        seven_days_ago = list(map(get_created_time, seven_days_ago_queryset)) if seven_days_ago_queryset else list()

        result = {
            '0':toady,
            '1':one_day_ago,
            '2':two_days_ago,
            '3':three_days_ago,
            '4':four_days_ago,
            '5':five_days_ago,
            '6':six_days_ago,
            '7':seven_days_ago,
        }

        return result


    class Meta:

        model = Project

        fields = (
            'application',
            'title',
            'sku',
            'hits_count',
            'hits_time',
            'user'
        )