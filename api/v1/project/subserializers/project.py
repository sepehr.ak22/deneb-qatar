from rest_framework import serializers

from project.models import Project

from accounts.models import Application


class ProjectSerializer(serializers.ModelSerializer):


    class Meta:

        model = Project

        fields = (
            'application',
            'title',
            'slug',
            'summary',
            'problem',
            'solution',
            'product',
            'customer',
            'business',
            'market',
            'competition',
            'vision',
            'founders',
            'cover',
            'is_published',
            'is_shown',
            'sku',
            'created',
            'modified',
        )