from rest_framework import routers

from . import views as view
from django.urls import re_path

app_name= 'api_v1_project'

router = routers.SimpleRouter()

# router.register(r'categories', view.CategoryViewSet)
router.register(r'projects', view.ProjectViewSet)
router.register(r'comments', view.CommentViewSet)

urlpatterns = [
    re_path(r'projects_hitcount', view.ProjectHitCountView.as_view(), name='projects_hitcount')
]

urlpatterns += router.urls