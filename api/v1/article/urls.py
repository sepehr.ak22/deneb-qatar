from rest_framework import routers

from . import views as view


app_name= 'api_v1_article'

router = routers.SimpleRouter()

router.register(r'posts', view.PostViewSet)

urlpatterns = router.urls