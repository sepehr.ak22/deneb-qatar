from rest_framework import serializers

from article.models import Post


class PostSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:

        model = Post

        fields = ('slug', 'title')