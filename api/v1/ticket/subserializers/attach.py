from rest_framework import serializers

from ticket.models import Attach


class AttachSerializer(serializers.ModelSerializer):

     class Meta:
          model = Attach
          fields = ('uploaded_file', 'ticket_id', 'comments_id', 'created',)
