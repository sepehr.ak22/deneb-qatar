from rest_framework import serializers

from ticket.models import Ticket
from django.contrib.auth.models import User

class TicketSerializer(serializers.ModelSerializer):

     raised_by = serializers.PrimaryKeyRelatedField(queryset = User.objects.all())


     class Meta:

          model = Ticket

          fields = ('subject', 'message',
                    'priority_status', 'answer_status',
                    'department_id', 'raised_by', 'created',
                   )
