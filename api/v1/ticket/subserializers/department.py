from rest_framework import serializers

from ticket.models import Department


class DepartmentSerializer(serializers.ModelSerializer):
     
     class Meta:
          model = Department
          fields = ('manager_id', 'users', 'name', 'description', 'joined_date', 'official',)
