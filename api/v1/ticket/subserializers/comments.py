from rest_framework import serializers

from ticket.models import Comments


class CommentsSerializer(serializers.ModelSerializer):
  
     class Meta:
          model = Comments
          fields = ('parent', 'message', 'ticket_id', 'staff_id',)
