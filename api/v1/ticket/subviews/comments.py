from django.db.models.query import QuerySet

from django.db.models import Q

from rest_framework import viewsets

from rest_framework.decorators import action

from rest_framework.response import Response

from rest_framework.permissions import (
    IsAuthenticated,
    DjangoModelPermissions
)

from painless.permissions import (
    AppViewPermission,
    IsAllowToViewAttachs
)

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)

from django_filters.rest_framework import DjangoFilterBackend

from functools import reduce

from ticket.models import (
    Department,
    Ticket,
    Comments,
)

from api.v1.ticket.serializers import (
    CommentsSerializer,
    AttachSerializer
)


class CommentsViewSet(viewsets.ModelViewSet):

    queryset = Comments.objects.all()

    serializer_class = CommentsSerializer

    permission_classes = [
        IsAuthenticated,
        DjangoModelPermissions,
    ]
    
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_fields = ['ticket_id','parent__id']
    ordering_fields = ['created',]
    search_fields = ['parent__id',]


    def permission_status(self, request, model_cls):
        
        return AppViewPermission().has_permission(request, model_cls)


    def get_department_ids(self, request):

        department_ids_query = Department.objects.filter(Q(manager_id=request.user) | \
                                                         Q(users=request.user)
                                                        ).values_list('id', flat = True)

        return department_ids_query
    

    def get_comments_query(self, request):

        department_ids = self.get_department_ids(request)

        comments_query_list = list(map(lambda ticket:ticket.replies.all(),
                                   Ticket.objects.filter(Q(department_id__in = department_ids) | \
                                                         Q(raised_by=request.user)
                                                        )
                                      )
                                  )

        if comments_query_list:

            comments_query = reduce(lambda query1, query2:(query1|query2), 
                                    comments_query_list).distinct()
        
        else:

            comments_query = Comments.objects.none()

        return comments_query
    

    def get_queryset(self):

        assert self.queryset is not None, (
            "'%s' should either include a `queryset` attribute, "
            "or override the `get_queryset()` method."
            % self.__class__.__name__
        )

        if self.permission_status(self.request, self.queryset.model):

            queryset = self.queryset

            if isinstance(queryset, QuerySet):

                queryset = queryset.all()
        else:

            queryset = self.get_comments_query(self.request)

        return queryset 


    @action(detail=False, methods = ['get'])
    def owners(self, request, pk = None):
        comments = self.get_queryset().filter(staff_id = request.user)
        serializer = self.get_serializer_class()(comments, many = True)
        return Response(serializer.data)

    
    @action(detail=False, methods = ['get'])
    def othercomments(self, request, pk = None):
        comments = self.get_queryset().filter(~Q(staff_id = request.user))
        serializer = self.get_serializer_class()(comments, many = True)
        return Response(serializer.data)
    
    
    @action(detail=True, methods = ['get'], permission_classes = [IsAllowToViewAttachs])
    def attachments(self, request, pk = None):
        comment = self.get_object()
        serializer = AttachSerializer(comment.attachs.all(), many=True)
        # print(serializer.data)
        return Response(serializer.data)

    
    @action(detail=True, methods = ['get'])
    def subcommnets(self, request, pk = None):
        comment = self.get_object()
        subcomments = self.get_queryset().filter(parent_id = comment.id)
        serializer = CommentsSerializer(subcomments, many=True)
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def newest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def oldest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').first()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)