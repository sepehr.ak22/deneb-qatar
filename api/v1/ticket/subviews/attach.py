from django.db.models.query import QuerySet

from django.db.models import Q

from rest_framework import viewsets

from rest_framework.decorators import action

from rest_framework.response import Response

from rest_framework.permissions import (
    IsAuthenticated,
    DjangoModelPermissions
)

from painless.permissions import AppViewPermission

from rest_framework.filters import OrderingFilter

from django_filters.rest_framework import DjangoFilterBackend

from functools import reduce

from ticket.models import (
    Department,
    Ticket,
    Comments,
    Attach
)

from api.v1.ticket.serializers import AttachSerializer


class AttachViewSet(viewsets.ModelViewSet):

    queryset = Attach.objects.all()

    serializer_class = AttachSerializer

    permission_classes = [
        IsAuthenticated,
        DjangoModelPermissions,
        
    ]

    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filter_fields = ['comments_id', 'ticket_id']
    ordering_fields = ['created',]


    def permission_status(self, request, model_cls):
        
        return AppViewPermission().has_permission(request, model_cls)


    def get_department_ids(self, request):

        department_ids_query = Department.objects.filter(Q(manager_id=request.user) | \
                                                         Q(users=request.user)
                                                        ).values_list('id', flat = True)

        return department_ids_query
    

    def get_comments_query(self, request):

        department_ids = self.get_department_ids(request)

        comments_query_list = list(map(lambda ticket:ticket.replies.all(),
                                       Ticket.objects.filter(Q(department_id__in = department_ids) | \
                                                             Q(raised_by=request.user)
                                                            )
                                      )
                                  )

        if comments_query_list:

            comments_query = reduce(lambda query1, query2:(query1|query2), 
                                    comments_query_list).distinct()
        
        else:

            comments_query = Comments.objects.none()

        return comments_query
    

    def get_attachs_query(self, request, comments=None):
        
        comments_query = self.get_comments_query(request) if comments is None else comments
        
        attachs_query_list = list(map(lambda comment: comment.attachs.all(),
                                      comments_query
                                     )
                                 )
        
        if attachs_query_list:

            attachs_query = reduce(lambda query1, query2:(query1|query2), 
                                attachs_query_list).distinct()
       
        else:

            attachs_query = Attach.objects.none()
        
        return attachs_query    


    def get_queryset(self):

        assert self.queryset is not None, (
            "'%s' should either include a `queryset` attribute, "
            "or override the `get_queryset()` method."
            % self.__class__.__name__
        )

        if self.permission_status(self.request, self.queryset.model):

            queryset = self.queryset

            if isinstance(queryset, QuerySet):

                queryset = queryset.all()
                
        else:

            queryset = self.get_attachs_query(self.request)

        return queryset 


    @action(detail=False, methods = ['get'])
    def owners(self, request, pk = None):
        comments = Comments.objects.filter(staff_id = request.user)
        attachs = self.get_attachs_query(request, comments)
        serializer = self.get_serializer_class()(attachs, many = True)
        return Response(serializer.data)

    
    @action(detail=False, methods = ['get'])
    def otherattachs(self, request, pk = None):
        comments = Comments.objects.filter(~Q(staff_id = request.user))
        attachs = self.get_attachs_query(request, comments)
        serializer = self.get_serializer_class()(attachs, many = True)
        return Response(serializer.data)
    
    
    @action(detail = False, methods = ['get'])
    def newest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def oldest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').first()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)