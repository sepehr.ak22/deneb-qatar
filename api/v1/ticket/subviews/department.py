from rest_framework import viewsets

from rest_framework.decorators import action

from rest_framework.response import Response

from rest_framework.permissions import (
    IsAuthenticated,
    DjangoModelPermissions
)

from painless.permissions import IsManagerOrMember

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)

from django_filters.rest_framework import DjangoFilterBackend

from ticket.models import Department

from api.v1.ticket.serializers import (
    DepartmentSerializer,
    TicketSerializer
)


class DepartmentViewSet(viewsets.ModelViewSet):

    queryset = Department.objects.all()

    serializer_class = DepartmentSerializer

    permission_classes = [
        IsAuthenticated,
        DjangoModelPermissions,
    ]

    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_fields = ['official',]
    ordering_fields = ['created',]
    search_fields = ['name',]


    @action(detail=True, methods = ['get'], permission_classes=[IsManagerOrMember])
    def tickets(self, request, pk = None):
        department = self.get_object()
        serializer = TicketSerializer(department.tickets.all(), many = True)
        return Response(serializer.data)