from django.db.models.query import QuerySet

from django.db.models import Q

from rest_framework import viewsets

from rest_framework.decorators import action

from rest_framework.response import Response

from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.permissions import (
    IsAuthenticated,
    DjangoModelPermissions
)

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)

from painless.permissions import (
    AppViewPermission,
    IsAllowToViewComments
)

from ticket.models import (
    Department,
    Ticket
)

from api.v1.ticket.serializers import (
    CommentsSerializer,
    TicketSerializer
)


class TicketViewSet(viewsets.ModelViewSet):

    queryset = Ticket.objects.all()

    serializer_class = TicketSerializer

    permission_classes = [
        IsAuthenticated,
        DjangoModelPermissions,
    ]

    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_fields = ['department_id', 'answer_status', 'priority_status']
    ordering_fields = ['created',]
    search_fields = ['subject',]


    def permission_status(self, request, model_cls):
        
        return AppViewPermission().has_permission(request, model_cls)


    def get_department_ids(self, request):

        department_ids_query = Department.objects.filter(Q(manager_id=request.user) | \
                                                         Q(users=request.user)
                                                        ).values('id')

        department_ids = [d_id['id'] for d_id in department_ids_query]

        return department_ids
    

    def get_ticket_query(self, request):

        department_ids = self.get_department_ids(request)

        ticket_query = Ticket.objects.filter(Q(department_id__in = department_ids) | \
                                             Q(raised_by=request.user)
                                            )
        

        return ticket_query
    

    def get_queryset(self):

        assert self.queryset is not None, (
            "'%s' should either include a `queryset` attribute, "
            "or override the `get_queryset()` method."
            % self.__class__.__name__
        )

        if self.permission_status(self.request, self.queryset.model):

            queryset = self.queryset

            if isinstance(queryset, QuerySet):

                queryset = queryset.all()
        else:

            queryset = self.get_ticket_query(self.request)

        return queryset 


    @action(detail=False, methods = ['get'])
    def owners(self, request, pk = None):
        tickets = self.get_queryset().filter(raised_by = request.user)
        serializer = self.get_serializer_class()(tickets, many = True)
        return Response(serializer.data)

    
    @action(detail=False, methods = ['get'])
    def othertickets(self, request, pk = None):
        tickets = self.get_queryset().filter(~Q(raised_by = request.user))
        serializer = self.get_serializer_class()(tickets, many = True)
        return Response(serializer.data)

    
    @action(detail=True, methods = ['get'], permission_classes = [IsAllowToViewComments])
    def comments(self, request, pk = None):
        ticket = self.get_object()
        serializer = CommentsSerializer(ticket.comments.all(), many = True)
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def newest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def oldest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').first()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)