from rest_framework import routers

from django.urls import re_path

from . import views


router = routers.SimpleRouter()

router.register(r'departments', views.DepartmentViewSet)
router.register(r'tickets', views.TicketViewSet)
router.register(r'comments', views.CommentsViewSet)
router.register(r'attachs', views.AttachViewSet)

urlpatterns = router.urls