from rest_framework import serializers

from faq.models import Faq


class FaqSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:

        model = Faq

        fields = ('slug', 'title', 'category', 'priority', 'description' )