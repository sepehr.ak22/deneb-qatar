from rest_framework import serializers

from faq.models import FaqCategory


class FaqCategorySerializer(serializers.HyperlinkedModelSerializer):

    
    class Meta:

        model = FaqCategory

        fields = ('slug', 'title', 'priority')