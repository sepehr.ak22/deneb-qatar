from rest_framework import routers

from . import views as view


app_name= 'api_v1_faq'

router = routers.SimpleRouter()

router.register(r'faqs', view.FaqViewSet)
router.register(r'faq_categories', view.FaqCategoryViewSet)

urlpatterns = router.urls