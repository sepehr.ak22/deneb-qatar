from django.db.models.query import QuerySet

from rest_framework.viewsets import ModelViewSet

from rest_framework.decorators import action

from rest_framework.response import Response

from django_filters.rest_framework import DjangoFilterBackend

from faq.models import FaqCategory

from api.v1.faq.serializers import FaqCategorySerializer

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)

from rest_framework.permissions import (
    IsAuthenticated,
    DjangoModelPermissions
)

from api.v1.faq.serializers import FaqSerializer


class FaqCategoryViewSet(ModelViewSet):

    queryset = FaqCategory.objects.all()
    
    serializer_class = FaqCategorySerializer

    permission_classes = [
        IsAuthenticated,
        DjangoModelPermissions
    ]

    filter_backends = [SearchFilter, OrderingFilter]
    ordering_fields = ['created', 'priority']
    search_fields = ['title',]

    
    @action(detail=True, methods = ['get'])
    def faqs(self, request, pk = None):
        category = self.get_object()
        serializer = FaqSerializer(category.faqs.all(),
                                   many = True,
                                   context={'request': request}
                                  )
        return Response(serializer.data)
    
    
    @action(detail = False, methods = ['get'])
    def newest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').last()
        serializer = self.get_serializer_class()(newest, context={'request': request})
        return Response(serializer.data)


    @action(detail = False, methods = ['get'])
    def oldest(self, request, pk = None):
        newest = self.get_queryset().order_by('created').first()
        serializer = self.get_serializer_class()(newest, context={'request': request})
        return Response(serializer.data)