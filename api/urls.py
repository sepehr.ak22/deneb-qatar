from django.urls import include, re_path

urlpatterns = [
    # global
    re_path(r'^', include('api.global.urls')),
    # v1
    re_path(r'^v1/accounts/', include('api.v1.accounts.urls')),
    re_path(r'^v1/ticket/', include('api.v1.ticket.urls')),
    re_path(r'^v1/faq/', include('api.v1.faq.urls')),
    re_path(r'^v1/knowledges_base/', include('api.v1.knowledgebase.urls')),
    re_path(r'^v1/article/', include('api.v1.article.urls')),
    re_path(r'^v1/project/', include('api.v1.project.urls')),
    re_path(r'^v1/blog/', include('api.v1.blog.urls')),

    # v2

]
