from django.urls import re_path, include

from rest_framework_jwt.views import (
    obtain_jwt_token,
    refresh_jwt_token,
    verify_jwt_token    
)

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)

from rest_framework.authtoken.views import obtain_auth_token 

from .jwt.blacklist import LogoutAndBlacklistRefreshTokenView

from painless.swagger import get_swagger_view
schema_view = get_swagger_view(title='Deneb API Documentation')

urlpatterns = [
    re_path(r'^auth/', include('rest_framework.urls')),

    re_path(r'^documentation/', schema_view),

    re_path(r'^security/device/token/obtain/$', obtain_auth_token),

    re_path(r'^security/token/obtain/$', obtain_jwt_token),
    re_path(r'^security/token/refresh/$', refresh_jwt_token),
    re_path(r'^security/token/verify/$', verify_jwt_token),
    
    re_path(r'^security/token/create/$', TokenObtainPairView.as_view(), name='token_obtain'),
    re_path(r'^security/token/illuminate/$', TokenRefreshView.as_view(), name='token_refresh'),
    re_path(r'^security/token/check/$', TokenVerifyView.as_view(), name='token_verify'),
    re_path(r'^security/token/blacklist/$', LogoutAndBlacklistRefreshTokenView.as_view(), name='blacklist'),
]
