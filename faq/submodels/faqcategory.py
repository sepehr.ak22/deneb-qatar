from django.db import models

from django.utils.translation import ugettext_lazy as _

from django.utils.text import slugify

from faq.submodels.general import (
    TimestampedModel,
    PropertiesModel
)


class FaqCategory(TimestampedModel, PropertiesModel):

    priority = models.PositiveIntegerField(_("Priority"))

    def save(self, *args, **kwargs):

        self.slug = slugify(self.title)

        super(FaqCategory, self).save(*args, **kwargs)


    class Meta:

        verbose_name = 'Faq Category'

        verbose_name_plural = 'Faq Categories'

        unique_together = ('title', 'priority')

        ordering = ('priority',)


    def __str__(self):

        return f'<{self.title}> FAQ Category'


    def __repr__(self):

        return f'<{self.title}> FAQ Category'