from django.db import models

from django.utils.translation import ugettext_lazy as _


class Identification(models.Model):

    sku = models.CharField(primary_key = False, max_length = 256,
                           editable = False, unique = True,
                           auto_created = True
                          )

    class Meta:

        abstract = True


class TimestampedModel(models.Model):
    
    created = models.DateTimeField(_("Created"), auto_now_add = True)

    modified = models.DateTimeField(_("Modified"), auto_now = True)


    class Meta:

        abstract = True


class PropertiesModel(models.Model):

    title = models.CharField(_("Title"), max_length=100)

    slug = models.SlugField(_("Slug"), max_length=120, editable = False)


    class Meta:
        
        abstract = True