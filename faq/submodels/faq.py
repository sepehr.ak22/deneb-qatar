from django.db import models

from django.utils.translation import ugettext_lazy as _

from django.utils.text import slugify

from faq.submodels.general import (
    TimestampedModel,
    PropertiesModel
)


class Faq(TimestampedModel, PropertiesModel):

    description = models.TextField(_("Description"), editable = True)

    category = models.ForeignKey('FaqCategory', verbose_name = _("FAQ Category"),
                                 related_name = 'faqs',
                                 on_delete = models.PROTECT
                                )

    priority = models.PositiveIntegerField(_("Priority"))

    def save(self, *args, **kwargs):

        self.slug = slugify(self.title)

        super(Faq, self).save(*args, **kwargs)


    class Meta:

        verbose_name = 'Faq'

        verbose_name_plural = 'Faqs'

        unique_together = ('category', 'priority')

        ordering = ('priority',)


    def __str__(self):

        return f'<{self.title}> of <{self.category.title}> FAQ Category'


    def __repr__(self):

        return f'<{self.title}> of <{self.category.title}> FAQ Category'