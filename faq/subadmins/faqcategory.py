from django.contrib import admin

from faq.models import FaqCategory


@admin.register(FaqCategory)
class FaqCategoryAdmin(admin.ModelAdmin):
    list_display = ('slug', 'title', 'created')
    list_filter = ('created',)
    search_fields = ('title',)
    ordering = ('-created',)