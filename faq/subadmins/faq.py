from django.contrib import admin

from faq.models import Faq


@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    list_display = ('slug', 'title', 'category', 'created')
    list_filter = ('created', 'category')
    search_fields = ('title',)
    ordering = ('-created',)