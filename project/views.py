from .models import Project

from django.http import Http404
from django.urls import reverse_lazy
from project.forms import (
    ProjectForm, 
    ProjectShownForm, 
    ProjectFinalizeForm
)

from django.shortcuts import get_object_or_404
from django.views.generic import View
from django.forms.models import model_to_dict
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.shortcuts import redirect, render
from accounts.models import Application

from hitcount.views import HitCountMixin
from hitcount.views import HitCountDetailView

from functools import reduce

class ProjectTemplateView(View):
    template_name = "dashboard/pages/project.html"
    view = {
        'page_name': 'Application',
        'remove_footer': True
    }

    def get(self, request, sku, sku_project, *args, **kwargs):
        project = get_object_or_404(Project, sku=sku_project)
        return render(request, self.template_name, {'project': project, 'view': self.view})


class ProjectView(LoginRequiredMixin, View):

    template_name = 'dashboard/pages/application-project-edit.html'
    view = {
        'page_name': "Project"
    }
    form = ProjectForm


    def validate_text_field(self):

        from django.core.exceptions import ValidationError
        
        except_values = ['<', '>', '/']

        summary = self.request.POST.get('summary')
        problem = self.request.POST.get('problem')
        solution = self.request.POST.get('solution')
        Product = self.request.POST.get('Product')
        Customer = self.request.POST.get('Customer')
        Business = self.request.POST.get('Business')
        Market = self.request.POST.get('Market')
        Competition = self.request.POST.get('Competition')
        Vision = self.request.POST.get('Vision')
        Founders = self.request.POST.get('Founders')

        texts_list = [summary, problem, 
                      solution, Product,
                      Customer, Business, 
                      Market, Competition, 
                      Vision, Founders
                    ]
                    
        texts = list(filter(lambda x: bool(x)==True, texts_list))
        
        for text in texts:

            for e_v in except_values:
            
                if e_v in text:

                    return False

        return True


    def check_clean_checkbox(self, request):

        return lambda file_filed: bool(request.FILES.get('{}-clean'.format(file_filed)))


    def check_file_field(self, request):

        return lambda file_filed: bool(request.FILES.get(file_filed))


    def file_filed_status(self, request, instance, value):

        check_clean_checkbox = self.check_clean_checkbox(request)
        check_file_field = self.check_file_field(request)
        
        if check_clean_checkbox(value) and not check_file_field(value):

            return None

        if not check_clean_checkbox(value) and check_file_field(value):

            return request.FILES.get(value)

        if not check_clean_checkbox(value) and not check_file_field(value):

            return getattr(instance, value)
    
    def is_user_valid(self, request, instance, *args, **kwargs):

        return True if request.user == instance.application.user else False


    def get(self, request, sku=None, sku_project=None, *args, **kwargs):

        project = get_object_or_404(Project, sku=sku_project)
        
        if self.is_user_valid(request, project):

            form = self.form(initial=model_to_dict(project))

            return render(request, self.template_name, {"form": form, "view": self.view, "project": project })
        
        else:
            raise Http404


    def post(self, request, sku=None, sku_project=None, *args, **kwargs):
        project = get_object_or_404(Project, sku=sku_project)
        form = self.form(request.POST, request.FILES, instance=project)

        if self.is_user_valid(self.request, project):

            if form.is_valid():

                if self.validate_text_field():

                    form.save(commit=False)

                    # print(request.POST)

                    project.title = request.POST.get('title')

                    project.summary = request.POST.get('summary')

                    project.summary_img = self.file_filed_status(request, project, 'summary_img')

                    project.problem = request.POST.get('problem')

                    project.problem_img = self.file_filed_status(request, project, 'problem_img')

                    project.solution = request.POST.get('solution')

                    project.solution_img = self.file_filed_status(request, project, 'solution_img')

                    project.Product = request.POST.get('Product')

                    project.product_img = self.file_filed_status(request, project, 'product_img')

                    project.Customer = request.POST.get('Customer')

                    project.customer_img = self.file_filed_status(request, project, 'customer_img')

                    project.Business = request.POST.get('Business')

                    project.business_img = self.file_filed_status(request, project, 'business_img')

                    project.Market = request.POST.get('Market')

                    project.market_img = self.file_filed_status(request, project, 'market_img')

                    project.Competition = request.POST.get('Competition')

                    project.competition_img = self.file_filed_status(request, project, 'competition_img')

                    project.Vision = request.POST.get('Vision')

                    project.vision_img = self.file_filed_status(request, project, 'vision_img')

                    project.Founders = request.POST.get('Founders')

                    project.founders_img = self.file_filed_status(request, project, 'founders_img')

                    project.video = self.file_filed_status(request, project, 'video')

                    project.cover = self.file_filed_status(request, project, 'cover')

                    project.is_shown = False

                    project.save()
                    
                    messages.success(request, "Your project has been changed.")

                    # import pdb ; pdb.set_trace()
                    return redirect('dashboard:application-edit', project.application.sku)

                else:

                    messages.error(request, "You can't use from < > / characters.")
                    
                    return redirect('dashboard:application-edit', project.application.sku)

            else:

                for f, m in form.errors.get_json_data().items():

                    message = m[0]['message']

                    field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   

                    messages.error(request, '{}: {}'.format(field, message))

                return redirect("dashboard:application-edit", project.application.sku)

        else:

            messages.error(request, "Some Error has been occured.")

            return redirect('dashboard:application-edit', project.application.sku)


class ProjectFinalizeView(LoginRequiredMixin, View):

    view = {
        'page_name': "Project"
    }
    form = ProjectFinalizeForm

    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.user else False

    def is_published_valid(self, request, instance):

        if request.POST.get('is_published') == 'true':
            # import pdb; pdb.set_trace()
            # instance.has_product and instance.qatari_partnership
            if  instance.title and \
                instance.summary and \
                instance.problem and \
                instance.solution and \
                instance.product and \
                instance.customer and \
                instance.business and \
                instance.market and \
                instance.vision and \
                instance.founders and \
                instance.cover and \
                instance.video:
                # import pdb; pdb.set_trace()
                messages.info(request, 'Project\'s detail has been changed')

                # if all fields are set
                return True

            else:

                messages.warning(request, 'Project\'s fields must be filled')

                # if one of field are none
                return False
        else:

            # if is_published set to false
            return False


    def get(self, request, sku, sku_project, *args, **kwargs):
        return redirect("dashboard:home")

    def post(self, request, sku, sku_project, *args, **kwargs):

        project = Project.objects.get(sku = sku_project)

        form = self.form(request.POST, instance=project)

        # import pdb; pdb.set_trace()
        if self.is_user_valid(request, project.application):

            if self.is_published_valid(request, project):
                # import pdb; pdb.set_trace()

                if form.is_valid():

                    messages.success(request, "Your project is finalized.")

                    project.is_published = True
                    project.save()

                    return redirect("dashboard:application-edit", project.application.sku)

                else:

                    for f, m in form.errors.get_json_data().items():

                        message = m[0]['message']

                        field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   

                        messages.error(request, '{}: {}'.format(field, message))

                    return redirect("dashboard:application-edit", project.application.sku)

        else:

            raise Http404

        # return redirect("dashboard:application-project-edit", project.application.sku, project.sku)
        return redirect("dashboard:application-edit", project.application.sku)


class ProjectShownView(LoginRequiredMixin, View):

    view = {
        'page_name': "Project"
    }
    form = ProjectShownForm

    def is_user_valid(self, request, instance, *args, **kwargs):
        return True if request.user == instance.application.user else False

    def get(self, request, sku, sku_project, *args, **kwargs):
        return redirect("dashboard:home")

    def post(self, request, sku, sku_project, *args, **kwargs):

        project = get_object_or_404(Project, sku = sku_project)

        form = self.form(request.POST, instance=project)

        if self.is_user_valid(request, project):

            if project.is_published:

                if form.is_valid():
                    if request.POST.get('is_shown') == 'true':
                        project.is_shown = True
                        messages.info(request, 'Your project has been shown in site.')
                    else:
                        project.is_shown = False
                        messages.info(request, 'Your project has been hidden from site.')

                    project.save()

                    

                    return redirect("dashboard:application-edit", project.application.sku)
                
                else:

                    for f, m in form.errors.get_json_data().items():

                        message = m[0]['message']

                        field = ' '.join(map(lambda x: str(x.capitalize()), f.split('_')))   

                        messages.error(request, '{}: {}'.format(field, message))

                    # return redirect("dashboard:application-project-edit", project.application.sku, project.sku)
                    return redirect("dashboard:application-edit", project.application.sku)

            else:

                messages.error(request, 'At the first you must published your project')
        else:

            raise Http404

        # return redirect("dashboard:application-project-edit", project.application.sku, project.sku)
        return redirect("dashboard:application-edit", project.application.sku)