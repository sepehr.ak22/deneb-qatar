from django.contrib import admin
from .models import Project 
# Register your models here.

@admin.register(Project)
class Admin(admin.ModelAdmin):
    list_display =('sku', 'title','is_shown', 'application', 'is_published')
    list_editable = ('is_shown', 'title')
    list_filter = ('is_shown',)
    search_fields =('title',)
    

    # onja baya har akse mortabet ba har safharo tarif