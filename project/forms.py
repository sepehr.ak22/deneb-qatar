from django import forms

from project.models import Project
from ckeditor.widgets import CKEditorWidget

class ProjectForm(forms.ModelForm):
    
    class Meta:

        model = Project

        fields = ('title',
                  'summary',
                  'summary_img',
                  'cover',
                  'problem',
                  'problem_img',
                  'solution',
                  'solution_img',
                  'product',
                  'product_img',
                  'customer',
                  'customer_img',
                  'business',
                  'business_img',
                  'market',
                  'market_img',
                  'competition',
                  'competition_img',
                  'vision',
                  'vision_img',
                  'founders',
                  'founders_img',
                  'video'
                 )

    

class ProjectFinalizeForm(forms.ModelForm):

    
    class Meta:

        model = Project

        fields = ('is_published',)



class ProjectShownForm(forms.ModelForm):

    
    class Meta:

        model = Project

        fields = ('is_shown',)