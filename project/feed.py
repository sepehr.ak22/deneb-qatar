from django.contrib.syndication.views import Feed

from django.template.defaultfilters import truncatewords

from project.models import Project

from django.urls import reverse


class ProjectFeed(Feed):

    title = "Projects"

    link = ""

    description = "New Projects"


    def items(self):

        return Project.objects.filter(is_shown = True)


    def item_title(self, item):

        return item.title


    def item_description(self, item):

        return truncatewords(item.summary, 30)


    def item_link(self, item):

        return reverse('api_v1_project:project-detail', args=[item.slug])