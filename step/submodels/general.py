from django.db import models

import secrets


class TimestampedModel(models.Model):

    created = models.DateTimeField(auto_now_add = True)

    modified = models.DateTimeField(auto_now = True)


    class Meta:

        abstract = True


class Status(models.Model):

    SECTIONS =( 
        ("1", "Profile"), 
        ("2", "Application"), 
        ("3", "Project"), 
        ("4", "Team"), 
        ("5", "Preview"), 
        ("6", "Result"), 
    ) 

    step = models.CharField(max_length = 1, choices = SECTIONS, default = '1')

    STATES =( 
        ("t", "task"), 
        ("i", "in_Progress"), 
        ("c", "Complete"), 
    ) 

    state = models.CharField(max_length = 1, choices = STATES, default = 'i')


    class Meta:
        
        abstract = True