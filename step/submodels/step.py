from django.db import models

from django.conf import settings

from django.utils.translation import ugettext_lazy as _

from django.utils.text import slugify

from step.submodels.general import (
    TimestampedModel,
    Status
)


class Step(TimestampedModel, Status):

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             verbose_name = _("User"),
                             related_name = 'steps',
                             on_delete = models.PROTECT
                            )

    application = models.ForeignKey('accounts.Application',
                                    verbose_name = _("Application"), 
                                    related_name = 'steps',
                                    on_delete = models.PROTECT,
                                    blank = True, null = True
                                   )

    refer_to = models.URLField(max_length = 300, blank = True, null = True)


    class Meta:

        verbose_name = 'Step'
        verbose_name_plural = 'Steps'
        ordering = ('step',)


    def __str__(self):

        return f'<{self.state}>  <{self.user.username}>'


    def __repr__(self):

        return f'<{self.state}>  <{self.user.username}>'