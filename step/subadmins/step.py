from django.contrib import admin

from step.models import Step


@admin.register(Step)
class StepAdmin(admin.ModelAdmin):
    list_display = ('user',
                    'application', 'refer_to',
                    'step', 'state',
                    'created'
                   )
    list_filter = ('created', 'state', 'step')
    search_fields = ('user_username',)
    ordering = ('-created',)