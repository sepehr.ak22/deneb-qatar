from django.shortcuts import render
from django.views.generic import TemplateView,ListView,DetailView,CreateView

from .models import Post , Category

from django.urls import reverse_lazy



from django.shortcuts import get_object_or_404
from django.views.generic import View

class BlogTemplateView(ListView):
    template_name = 'pages/blog/posts.html'
    page_name = 'home'
    context_object_name = 'character_series_list'
    def get_context_data(self, **kwargs):
        context = super(BlogTemplateView, self).get_context_data(**kwargs)
        context.update({
            'post_show': Post.objects.all(),
            'categories': Category.objects.all(),
        })
        return context

    def get_queryset(self):
        return Post.objects.order_by('title')





class PostTemplateView(View):
    template_name = 'page/blog/post.html'
    view = {
        'page_name': 'blog_page',
        'remove_footer': True
    }
    slug_field = 'slug'
    def get(self, request, slug, *args, **kwargs):
        post = get_object_or_404(Post, slug=slug)
        category = get_object_or_404(Category, slug=slug)
        return render(request, self.template_name, {'category':category,'post': post, 'view': self.view})

