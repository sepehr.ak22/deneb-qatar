from django.contrib import admin
from .models import Post , Category



@admin.register(Post)
class Admin(admin.ModelAdmin):
    pass

@admin.register(Category)
class Admin(admin.ModelAdmin):
    pass