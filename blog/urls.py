from django.urls import path, re_path, include
from . import views

app_name = 'blog'
urlpatterns = [
    re_path(r'^$', views.BlogTemplateView.as_view(), name='project') ,
    path('<str:slug>/',views.PostTemplateView.as_view(),name='blog_post'),
]
