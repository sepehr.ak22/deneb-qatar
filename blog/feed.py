from django.contrib.syndication.views import Feed

from django.template.defaultfilters import truncatewords

from blog.models import Post

from django.urls import reverse


class PostFeed(Feed):

    title = "Posts"

    link = ""

    description = "New Posts"


    def items(self):

        return Post.objects.all()


    def item_title(self, item):

        return item.title


    def item_description(self, item):

        return truncatewords(item.main_text, 30)


    def item_link(self, item):

        return reverse('api_v1_blog:post-detail', args=[item.slug])