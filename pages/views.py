from django.shortcuts import render
from django.views.generic import (
    TemplateView,
    CreateView,
    DetailView
)

from django.utils.translation import ugettext_lazy as _

from django.contrib.messages.views import SuccessMessageMixin

from django.urls import reverse_lazy

from .models import Banner
from .models import Site
from .models import Page
from accounts.models import Application
from project.models import Project
from blog.models import Post

from .models import Contact
from .forms import ContactForm

from hitcount.views import HitCountDetailView

class HomeView(SuccessMessageMixin, CreateView):
    template_name = "pages/index.html"
    page_name = 'Home'
    model = Contact
    fields = ('first_name', 'last_name', 'email', 'phone', 'project', 'country', 'message',)
    success_url = reverse_lazy('pages:contact')
    success_message = _("Your message has been saved. We will contact you soon.")

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        apps = Application.objects.all()
        context['projects'] = Project.objects.filter(application__in = apps, is_shown = True)
        context['recent_posts'] = Post.objects.all().order_by('-created')[:3]

        return context

    def recaptcha_validation(self, form):
        from django.conf import settings
        import requests

        secret_key = settings.RECAPTCHA_SECRET_KEY

        data = {
            'response': form.data['g-recaptcha-response'],
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False

    def form_valid(self, form):
        form.save(commit=False)

        if self.recaptcha_validation(form):
            return super(HomeView, self).form_invalid(form)
        else:
            form.save()
            return super(HomeView, self).form_valid(form)

class ServiceView(TemplateView):
    template_name = "pages/service.html"
    page_name = 'Services'

    def get_context_data(self, **kwargs):
        context = super(ServiceView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        context['page'] = Page.objects.get(title = 'Service')
        return context

class AboutView(TemplateView):
    template_name = "pages/about.html"
    page_name = 'About'

    def get_context_data(self, **kwargs):
        
        context = super(AboutView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        context['page'] = Page.objects.get(title = 'About')
        return context

class PortfolioView(TemplateView):
    template_name = "pages/portfolio.html"
    page_name = 'Portfolio'

    def get_context_data(self, **kwargs):
        context = super(PortfolioView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        context['page'] = Page.objects.get(title = 'Portfolio')

        apps = Application.objects.all()
        context['projects'] = Project.objects.filter(application__in = apps, is_shown = True)
        # import pdb ; pdb.set_trace()
        return context

class PortfolioDetailView(HitCountDetailView):
    template_name = "pages/portfolio-detail.html"
    page_name = 'Portfolio'
    model = Project
    slug_field = 'slug'
    context_object_name  = 'project'

    count_hit = True

    def get_context_data(self, **kwargs):
        context = super(PortfolioDetailView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        context['recent_projects'] = Project.objects.filter(is_shown = True).order_by('-created')[:5]
        context['page'] = Page.objects.get(title = 'Portfolio')
        return context

class ContactView(SuccessMessageMixin, CreateView):
    template_name = "pages/contact.html"
    page_name = 'Contact'
    model = Contact
    fields = ('first_name', 'last_name', 'email', 'phone', 'project', 'country', 'message',)
    success_url = reverse_lazy('pages:contact')
    success_message = _("Your message has been saved. We will contact you soon.")

    def get_context_data(self, **kwargs):
        # from sentry_sdk import capture_message
        # import logging
        # capture_message("My DEBUG Message to sentry!", level="debug")
        # capture_message("My INFO Message to sentry!", level="info")
        # capture_message("My CRITICAL Message to sentry!", level="critical")
        # capture_message("My ERROR Message to sentry!", level="error")
        # logging.debug("My DEBUG Message to sentry!")
        # logging.info("My INFO Message to sentry!")
        # logging.warning("My WARNING Message to sentry!")
        # logging.critical("My CRITICAL Message to sentry!")
        # logging.error("My ERROR Message to sentry!")
        context = super(ContactView, self).get_context_data(**kwargs)
        context['site'] = Site.objects.first()
        context['page'] = Page.objects.get(title = 'Contact')
        return context

    def recaptcha_validation(self, form):
        from django.conf import settings
        import requests
        
        secret_key = settings.RECAPTCHA_SECRET_KEY
        
        data = {
            'response': form.data['g-recaptcha-response'],
            'secret': secret_key
        }

        resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result_json = resp.json()

        return True if result_json.get('success') else False
        
    def form_valid(self, form):
        form.save(commit=False)
        
        if self.recaptcha_validation(form):
            return super(ContactView, self).form_invalid(form)
        else:
            form.save()
            return super(ContactView, self).form_valid(form)

