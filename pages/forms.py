from django import forms
from .models import Contact

from django_countries.widgets import CountrySelectWidget

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['first_name', 'last_name', 'email', 'phone', 'project', 'country', 'message',]

        widgets = {
            'country': CountrySelectWidget(),
        }