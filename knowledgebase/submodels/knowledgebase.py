from django.db import models

from django.utils.translation import ugettext_lazy as _

from django.utils.text import slugify

from faq.submodels.general import (
    TimestampedModel,
    PropertiesModel
)


class KnowledgeBase(TimestampedModel, PropertiesModel):

    description = models.TextField(_("Description"), editable = True)

    category = models.ForeignKey('KnowledgeBaseCategory',
                                 verbose_name = _("KnowledgeBase Category"),
                                 related_name = 'knowledges_base',
                                 on_delete = models.PROTECT
                                )

    priority = models.PositiveIntegerField(_("Priority"))


    def save(self, *args, **kwargs):

        self.slug = slugify(self.title)

        super(KnowledgeBase, self).save(*args, **kwargs)


    class Meta:

        verbose_name = 'Knowledge Base'

        verbose_name_plural = 'Knowledges Base'

        unique_together = ('category', 'priority')

        ordering = ('priority',)


    def __str__(self):

        return f'<{self.title}> of <{self.category.title}> KnowledgeBase Category'


    def __repr__(self):

        return f'<{self.title}> of <{self.category.title}> KnowledgeBase Category'