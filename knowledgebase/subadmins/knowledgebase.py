from django.contrib import admin

from knowledgebase.models import KnowledgeBase


@admin.register(KnowledgeBase)
class KnowledgeBaseAdmin(admin.ModelAdmin):
    list_display = ('slug', 'title', 'category', 'priority', 'created')
    list_filter = ('created', 'category')
    search_fields = ('title',)
    ordering = ('-created',)