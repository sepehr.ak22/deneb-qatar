from django.contrib import admin

from knowledgebase.models import KnowledgeBaseCategory


@admin.register(KnowledgeBaseCategory)
class KnowledgeBaseCategoryAdmin(admin.ModelAdmin):
    list_display = ('slug', 'title', 'priority', 'created')
    list_filter = ('created',)
    search_fields = ('title',)
    ordering = ('-created',)