from django.db import models
from django.core.validators import FileExtensionValidator
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.urls import reverse
from ckeditor_uploader.fields import RichTextUploadingField

class Post(models.Model):
    title = models.CharField(_("Title"), max_length=60)
    slug = models.SlugField(_("Slug"), max_length=60, editable = False, allow_unicode=True)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
       self.slug = slugify(self.title)
       super(Post, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("Article:post", kwargs={"slug": self.slug})






