from django.contrib import admin

from article.models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('slug', 'title', 'created')
    list_filter = ('created',)
    search_fields = ('title',)
    ordering = ('-created',)