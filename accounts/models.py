import secrets

from django.db import models
from django.core.validators import (
    RegexValidator,
    FileExtensionValidator,
)
from django.core.exceptions import ValidationError
from django.utils.text import slugify

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.db import models
from django_countries.fields import CountryField

from django.utils.translation import ugettext_lazy as _

from painless.upload_features.upload_path import images_upload_to


class Profile(models.Model):
    GENDERS = (
        ('m', _('Male')),
        ('f', _('Female')),
        ('o', _('Other')),
        ('p', _('Prefer Not to say')),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    
    id_card = models.FileField(verbose_name='Qid or Passport',
                               upload_to=images_upload_to,
                               null = True, blank = True, 
                               validators=[FileExtensionValidator(allowed_extensions=['pdf', 'jpg', 'png'])]
                              )


    phone_number = models.CharField(_("Phone Number"), max_length = 12, null=True, blank=True)

    width_field = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    cover = models.ImageField(_("Cover"), 
                                upload_to='site', 
                                height_field='height_field', 
                                width_field='width_field', 
                                validators=[FileExtensionValidator(allowed_extensions=['jpg', 'png'])],
                                max_length=100, 
                                null = True, 
                                blank = True)

    width_field_pic = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field_pic = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    picture = models.ImageField(_("Picture"), 
                                upload_to='site', 
                                height_field='height_field', 
                                width_field='width_field', 
                                validators=[FileExtensionValidator(allowed_extensions=['jpg', 'png'])],
                                max_length=100, 
                                null = True, 
                                blank = True)
    
    is_published = models.BooleanField(default = False)

    origin = CountryField(_("Country Origin"), null = True, blank = True)
    residence = CountryField(_("Country Residence"), null = True, blank = True)
    gender = models.CharField(_("Gender"), choices = GENDERS, max_length = 1, null = True, blank = True)
    bio = models.TextField(max_length=650, null = True, blank = True)
    location = models.CharField(_("Address"), max_length = 120, null = True, blank = True)
    email_confirmed = models.BooleanField(default=False)

    instagram = models.URLField(_("Instagram"), max_length=200, blank = True, null = True)
    facebook = models.URLField(_("Facebook"), max_length=200, blank = True, null = True)
    twitter = models.URLField(_("Twitter"), max_length=200, blank = True, null = True)

    modified = models.DateTimeField(_("Modified"), auto_now=True)
    
    class Meta:
        """Meta definition for Profile."""

        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')
    
    # def clean(self):
    #     if self.is_published:
    #         if not self.phone_number or not self.cover or not self.id_card or not self.picture or not self.origin or not self.residence or not self.gender or \
    #             not self.location or not self.bio:
    #             raise ValidationError("please fill all important informations.")
        

    def __str__(self):
        
        return self.user.get_full_name()

    def save(self, *args, **kwargs):
        
        # self.full_clean()
        super(Profile, self).save(*args, **kwargs)

class Responsiblity(models.Model):
    user = models.ForeignKey(User, related_name='responsiblities', on_delete=models.CASCADE)
    title = models.CharField(_("Title"), max_length = 100)
    slug = models.SlugField(_("Slug"), max_length = 120, editable = False)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        """Meta definition for Profile."""

        verbose_name = _('Responsibility')
        verbose_name_plural = _('Responsibilities')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Responsiblity, self).save(*args, **kwargs)

class Member(models.Model):
    first_name = models.CharField(_("First Name"), max_length = 100)
    last_name = models.CharField(_("Last Name"), max_length = 100)
    position = models.CharField(_("Position"), max_length = 100)
    phone_number = models.CharField(_("Phone Number"), max_length = 12, null=True, blank=True)
    email = models.EmailField(_('email address'), blank=True)

    width_field_pic = models.PositiveIntegerField(_("Width Field"), editable = False, null = True)
    height_field_pic = models.PositiveIntegerField(_("Height Field"), editable = False, null = True)
    picture = models.ImageField(_("Picture"), upload_to='site', height_field='height_field_pic', width_field='width_field_pic', max_length=100, null = True, blank = True)

    team = models.ForeignKey('Team', related_name='colleagues', on_delete=models.CASCADE)


    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    
    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __str__(self):
        return self.first_name

    def save(self, *args, **kwargs):
        super(Member, self).save(*args, **kwargs)

class Team(models.Model):
    name = models.CharField(_("Team Name"), max_length = 200)
    user = models.ForeignKey(User, related_name='teams', on_delete=models.CASCADE)
    project = models.OneToOneField('project.Project', related_name='team', on_delete=models.CASCADE,)
    is_published = models.BooleanField(_("Is published"), default=False)

    class Meta:
        """Meta definition for Profile."""
        verbose_name = _('Team')
        verbose_name_plural = _('Teams')

    def get_full_name(self):
        return '{}'.format(self.name)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(Team, self).save(*args, **kwargs)

class Experience(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    title = models.CharField(_("Title"), max_length = 70)
    company = models.CharField(_("Company"), max_length = 70)
    location = models.CharField(_("Location"), max_length = 120)

    from_date = models.DateField(_("From"))
    to_date = models.DateField(_("To"), null = True, blank = True)
    currently_work = models.BooleanField(_("Currently work here"), default = False)
    description = models.TextField(blank=True, null = True)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        """Meta definition for Experience."""

        verbose_name = _('Experience')
        verbose_name_plural = _('Experiences')

    def __str__(self):
        return self.title
    
    def save(self, *args, **kwargs):
        super(Experience, self).save(*args, **kwargs)

class Application(models.Model):
    INDUSTRIES = (
        ('Art', 'Art'),
        ('Aerospace industry', 'Aerospace industry'),
        ('Agriculture', 'Agriculture'),
        ('Fishing', 'Fishing'),
        ('Chemical', 'Chemical'),
        ('Computer', 'Computer'),
        ('Construction', 'Construction'),
        ('Defense', 'Defense'),
        ('Education', 'Education'),
        ('Entertainment', 'Entertainment'),
        ('Financial', 'Financial'),
        ('Food', 'Food'),
        ('Healthcare', 'Healthcare'),
        ('Hospitality', 'Hospitality'),
        ('Manufacturing', 'Manufacturing'),
        ('Media', 'Media'),
        ('Telecom', 'Telecom'),
        ('Transport', 'Transport'),
        ('Water', 'Water'),
        ('Sports', 'Sports'),
        ('E-Commerce', 'E-Commerce'),
        ('Tourism', 'Tourism'),
        ('Fashion', 'Fashion'),
        ('Marketing', 'Marketing'),
        ('HR', 'HR'),
    )

    deneb_resultS = (
        ('No Experience', _('No Experience')),
        ('Rejection', _('Rejection')),
        ('Incubation', _('Incubation')),
        ('Services', _('Services')),
        ('Techshow', _('Techshow')),
        ('Referal to other organization', _('Referal to other organization')),
    )

    SALE_RATE_TYPE = (
        ('m', _('Monthly')),
        ('y', _('Yearly')),
    )

    sku = models.CharField(max_length = 256, editable = False, unique = True)
    user = models.ForeignKey(User, related_name='applications', on_delete=models.CASCADE, null=True, blank=True)
    qatari_partnership = models.BooleanField(_("Qatari Partnership"), default = False, null=True, blank=True)
    website = models.URLField(_("Website"), max_length=200, null=True, blank=True)
    problem  = models.TextField(_("Problem"), null=True, blank=True)
    solution  = models.TextField(_("Solution"), null=True, blank=True)
    
    is_b2b = models.BooleanField(_("Is B2B"), default = False)
    is_b2c = models.BooleanField(_("Is B2C"), default = False)
    is_b2g = models.BooleanField(_("Is B2G"), default = False)
    is_b2b2c = models.BooleanField(_("Is B2B2C"), default = False)

    industry = models.CharField(_("Industry"), choices = INDUSTRIES, max_length = 30, null=True, blank=True)
    has_product = models.BooleanField(_("Has Product"), default = False, null=True, blank=True)

    sales_rate = models.PositiveIntegerField(_("Sales rate"), null=True, blank=True)
    sales_rate_type = models.CharField(_("Sales rate Type"), max_length=1, choices=SALE_RATE_TYPE, null=True, blank=True)

    deneb_result = models.CharField(_("Deneb Result"), choices=deneb_resultS, max_length = 35, null=True, blank=True)
    is_published = models.BooleanField(_("Finalize"), default = False)

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    # Kafil information
    kafil_fname = models.CharField(_("Kafil First Name"), max_length = 45, null=True, blank=True)
    kafil_lname = models.CharField(_("Kafil Last Name"), max_length = 45, null=True, blank=True)
    kafil_email = models.EmailField(_("Kafil Email"), max_length=254, null=True, blank=True)
    kafil_qatari_id = models.CharField(_("Qatari iD"), max_length = 60,
                        validators=[RegexValidator(r'^[0-9]*$', 'Only positive integer are allowed.')], 
                        null=True, blank=True
                    )
    
    class Meta:
        """Meta definition for Experience."""

        verbose_name = _('Application')
        verbose_name_plural = _('Applications')
        index_together = ('sku', 'is_published')
    # def clean(self):
    #     if self.is_published:  
    #         if not self.qatari_partnership or not self.website or not self.problem or not self.solution or not self.industry or not self.sales_rate or not self.deneb_result:
    #             raise ValidationError(_("please fill all important informations."))

    def __str__(self):
        # import pdb ; pdb.set_trace()
        # return self.user.email
        return self.sku
    
    def save(self, *args, **kwargs):
        # self.full_clean()
        if not self.pk:
            self.sku = secrets.token_hex(16)
        super(Application, self).save(*args, **kwargs)
 
class UniqueUserVisit(models.Model):
    user = models.ForeignKey(User, verbose_name=_(""), on_delete=models.SET_NULL, null=True)
    namespace = models.CharField(_("NameSpace"), max_length=50)
    view_name = models.CharField(_("View Name"), max_length=50)


    class Meta:
        verbose_name = _('Unique User Visit')
        verbose_name_plural = _('Unique User Visits')

    def __str__(self):
        return self.user.get_full_name()

    def save(self, *args, **kwargs):
       super(UniqueUserVisit, self).save(*args, **kwargs)

