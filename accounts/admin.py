from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.models import User

from .models import Responsiblity
from .models import Application
from .models import Profile
from .models import Experience
from .models import Team
from .models import Member, Application


admin.site.unregister(User)

@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    
    list_display = ('sku', 'user', 'qatari_partnership', 'industry', 'has_product', 'deneb_result', 'is_published', 'is_b2b', 'is_b2c', 'is_b2g', 'is_b2b2c', 'created')
    list_filter = ('qatari_partnership', 'industry', 'has_product', 'deneb_result', 'is_published', 'is_b2b', 'is_b2c', 'is_b2g', 'is_b2b2c', 'created', 'modified',)
    search_fields = ('sku',)


@admin.register(Responsiblity)
class ResponsiblityAdmin(admin.ModelAdmin):
    list_display = ('title', 'created', 'modified')
    list_filter = ('created', 'modified',)
    search_fields = ('title',)

class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'

class ExperienceInline(admin.StackedInline):
    model = Experience
    can_delete = False
    verbose_name_plural = 'Experience'
    fk_name = 'user'

class ApplicationInline(admin.StackedInline):
    model = Application
    can_delete = False
    verbose_name_plural = 'Application'
    fk_name = 'user'

# class TeamInline(admin.StackedInline):
#     model = Team
#     can_delete = False
#     verbose_name_plural = 'Team'
#     fk_name = 'user'
#     extra = 1

@admin.register(User)
class CustomUserAdmin(AuthUserAdmin):
    inlines = (ProfileInline,)

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'project')

@admin.register(Member)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'position', 'team')
    list_filter = ('team', 'position', 'created', 'modified')
    search_fields = ('first_name', 'last_name',)