from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms

from django_countries.widgets import CountrySelectWidget

from accounts.models import (
    Profile,
    Responsiblity,
    Experience,
    Team,
    Member,
    Application
)


import re

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )

class AccountProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ('user', 'phone_number', 'origin', 'residence', 'gender', 'bio', 'location', 'instagram', 'facebook', 'twitter', 'cover', 'picture')

class ApplicationForm(forms.ModelForm):

    TRUE_FALSE_CHOICES = (
        (True, 'Yes'),
        (False, 'No')
    )

    SALE_RATE_TYPE = (
        ('m', _('Monthly')),
        ('y', _('Yearly')),
    )

    has_product = forms.ChoiceField(choices = TRUE_FALSE_CHOICES, label=_("Has Product"), 
                                    initial='', widget=forms.Select(), required=True)

    qatari_partnership = forms.ChoiceField(choices = TRUE_FALSE_CHOICES, label=_("Qatari Partnership"), 
                                           initial='', widget=forms.Select(), required=True)

    sales_rate_type = forms.ChoiceField(choices = SALE_RATE_TYPE, label=_("Sales rate Type"), 
                                           initial='', widget=forms.Select(), required=True)


    class Meta:
        model = Application
        fields = ('user', 'qatari_partnership', 'website', 'problem', 'solution',
                 'is_b2b', 'is_b2c', 'is_b2g', 'is_b2b2c', 'industry', 'has_product',
                 'sales_rate', 'deneb_result', 'is_published', 'sales_rate_type',
                 'kafil_fname', 'kafil_lname', 'kafil_email', 'kafil_qatari_id')


class ApplicationPublishForm(forms.ModelForm):

    class Meta:
        
        model = Application

        fields = ('user', 'is_published')


class ProfileForm(forms.ModelForm):


    class Meta:

        model = Profile

        fields = ('user', 'id_card', 'phone_number',
                  'cover', 'picture', 'is_published', 'origin',
                  'residence', 'gender', 'bio', 'location',
                  'instagram', 'facebook', 'twitter',
                 )

        widgets = {'origin': CountrySelectWidget(),
                   'residence': CountrySelectWidget()
                  }

class ProfilePublishForm(forms.ModelForm):
    class Meta:
        model = Profile

        fields = ('user', 'is_published')
    
class ResponsiblityForm(forms.ModelForm):

    class Meta:

        model = Responsiblity

        fields = ('user', 'title')


class MemberForm(forms.ModelForm):

    class Meta:

        model = Member

        fields = ('first_name', 'last_name', 'team',
                  'position', 'phone_number', 'email', 'picture'  
                 )


class TeamForm(forms.ModelForm):

    class Meta:

        model = Team

        fields = ('name', 'project', 'user')


class TeamPublishedForm(forms.ModelForm):

    class Meta:

        model = Team

        fields = ('user', 'is_published')


class ExperienceForm(forms.ModelForm):

    class Meta:

        model = Experience

        fields = ('user',
                  'title',
                  'company',
                  'location',
                  'from_date',
                  'to_date',
                  'currently_work',
                  'description', 
                 )