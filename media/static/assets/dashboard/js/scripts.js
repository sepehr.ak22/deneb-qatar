/* Dore Theme Select & Initializer Script 

Table of Contents

01. Css Loading Util
02. Theme Selector And Initializer
*/

/* 01. Css Loading Util */
function loadStyle(href, callback) {
  for (var i = 0; i < document.styleSheets.length; i++) {
    if (document.styleSheets[i].href == href) {
      return;
    }
  }
  var head = document.getElementsByTagName("head")[0];
  var link = document.createElement("link");
  link.rel = "stylesheet";
  link.type = "text/css";
  link.href = href;
  if (callback) {
    link.onload = function () {
      callback();
    };
  }
  var mainCss = $(head).find('[href$="main.css"]');
  if (mainCss.length !== 0) {
    mainCss[0].before(link);
  } else {
    head.appendChild(link);
  }
}

/* 02. Theme Selector, Layout Direction And Initializer */
(function ($) {
  if ($().dropzone) {
    Dropzone.autoDiscover = false;
  }

  var themeColorsDom = '<div></div>';

  $("body").append(themeColorsDom);


  /* Default Theme Color, Border Radius and  Direction */
  var theme = "dore.light.orange.min.css";
  var direction = "ltr";
  var radius = "rounded";

  if (typeof Storage !== "undefined") {
    if (localStorage.getItem("dore-theme")) {
      theme = localStorage.getItem("dore-theme");
    } else {
      localStorage.setItem("dore-theme", theme);
    }
    if (localStorage.getItem("dore-direction")) {
      direction = localStorage.getItem("dore-direction");
    } else {
      localStorage.setItem("dore-direction", direction);
    }
    if (localStorage.getItem("dore-radius")) {
      radius = localStorage.getItem("dore-radius");
    } else {
      localStorage.setItem("dore-radius", radius);
    }
  }

  $(".theme-color[data-theme='" + theme + "']").addClass("active");
  $(".direction-radio[data-direction='" + direction + "']").attr("checked", true);
  $(".radius-radio[data-radius='" + radius + "']").attr("checked", true);
  $("#switchDark").attr("checked", theme.indexOf("dark") > 0 ? true : false);

  loadStyle("/static/assets/dashboard/css/" + theme, onStyleComplete);
  function onStyleComplete() {
    setTimeout(onStyleCompleteDelayed, 300);
  }

  function onStyleCompleteDelayed() {
    $("body").addClass(direction);
    $("html").attr("dir", direction);
    $("body").addClass(radius);
    $("body").dore();
  }

  $("body").on("click", ".theme-color", function (event) {
    event.preventDefault();
    var dataTheme = $(this).data("theme");
    if (typeof Storage !== "undefined") {
      localStorage.setItem("dore-theme", dataTheme);
      window.location.reload();
    }
  });

  $("input[name='directionRadio']").on("change", function (event) {
    var direction = $(event.currentTarget).data("direction");
    if (typeof Storage !== "undefined") {
      localStorage.setItem("dore-direction", direction);
      window.location.reload();
    }
  });

  $("input[name='radiusRadio']").on("change", function (event) {
    var radius = $(event.currentTarget).data("radius");
    if (typeof Storage !== "undefined") {
      localStorage.setItem("dore-radius", radius);
      window.location.reload();
    }
  });

  $("#switchDark").on("change", function (event) {
    var mode = $(event.currentTarget)[0].checked ? "dark" : "light";
    if (mode == "dark") {
      theme = theme.replace("light", "dark");
    } else if (mode == "light") {
      theme = theme.replace("dark", "light");
    }
    if (typeof Storage !== "undefined") {
      localStorage.setItem("dore-theme", theme);
      window.location.reload();
    }
  });

  $(".theme-button").on("click", function (event) {
    event.preventDefault();
    $(this)
      .parents(".theme-colors")
      .toggleClass("shown");
  });

  $(document).on("click", function (event) {
    if (
      !(
        $(event.target)
          .parents()
          .hasClass("theme-colors") ||
        $(event.target)
          .parents()
          .hasClass("theme-button") ||
        $(event.target).hasClass("theme-button") ||
        $(event.target).hasClass("theme-colors")
      )
    ) {
      if ($(".theme-colors").hasClass("shown")) {
        $(".theme-colors").removeClass("shown");
      }
    }
  });
})(jQuery);


// $('#step-list li.option').click(function () {
//   $(this).addClass('active');
//   var stepFor = $(this).attr('for');
//   $('.billing__main__content__forms__div').removeClass('active');
//   $('#' + stepFor).addClass('active');

//   if(stepFor == 'extra-info'){
//       $('#step-list li.warning').css('display','none');
//   }

//   var dataStep = $(this).attr('data-step');
//   $('#step-list li.option').each(function() {
//       if($(this).attr('data-step') > dataStep){
//           $(this).removeClass('active');
//       }else{
//           $(this).addClass('active');
//       }
//       if($(this).attr('data-step') >= dataStep){
//           $(this).removeClass('done');
//       }else{
//           $(this).addClass('done');
//       }
//   });
// });
$('.billing__main__content__forms__btn').click(function () {
  var formsBtnId = $(this).attr('data-continue');

  $("li[for = '" + formsBtnId + "']").trigger( "click" );


  var findLiStep = $("li[for = '" + formsBtnId + "']").attr('data-step');

  $('#step-list li.option').each(function() {
      if($(this).attr('data-step') >= findLiStep){
          $(this).removeClass('done');
      }else{
          $(this).addClass('done');
      }
  });


  var currentPage2 = window.location.href.split('#')[0];
  window.location.href = currentPage2 + '#' + formsBtnId;
});

$('#extra-info-view').click(function () {
  $('#step-list li.warning').css('display','none');
});

var currentPage = window.location.href.split('#')[1];
if (currentPage !== undefined && currentPage !== null && currentPage  !== 'undefined'){
  $('.billing__main__content__forms__div').removeClass('active');
  $('#' + currentPage).addClass('active');
  $("li[for = '" + currentPage + "']").addClass('active');

  if(currentPage == 'extra-info' || currentPage == 'payment-info' || currentPage == 'congratulations'){
      $('#step-list li.warning').css('display','none');
  }

  if($("li[for = '" + currentPage + "']")){
      var chooseStep = $("li[for = '" + currentPage + "']").attr('data-step');

      $('#step-list li.option').each(function() {
          if($(this).attr('data-step') > chooseStep){
              $(this).removeClass('active');
          }else{
              $(this).addClass('active');
          }
          if($(this).attr('data-step') >= chooseStep){
              $(this).removeClass('done');
          }else{
              $(this).addClass('done');
          }
      });
  }
}

$('.billing__main__search span').click(function () {
  if($(this).hasClass('clicked')){
      $(this).removeClass('clicked');
      $(this).html('YENİ AKTİVİTE ARA <i class="fa fa-angle-down" aria-hidden="true"></i>');
      $('.billing__main__header__default').addClass('active');
      $('.billing__main__header__search').removeClass('active');
  }
  else{
      $(this).addClass('clicked');
      $(this).html('ARAMAYI KAPAT <i class="fa fa-times" aria-hidden="true"></i>');
      $('.billing__main__header__default').removeClass('active');
      $('.billing__main__header__search').addClass('active');
  }
});