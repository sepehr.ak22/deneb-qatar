from rest_framework.permissions import BasePermission, DjangoModelPermissions

from rest_framework import exceptions

from ticket.models import Ticket

from api.v1.ticket.serializers import TicketSerializer


class IsAllowToViewTickets(BasePermission):
    
    def has_permission(self, request, view):
        return bool(request.user and request.user.has_perm('ticket.view_ticket')) 


class IsAllowToViewComments(BasePermission):
    
    def has_permission(self, request, view):
        return bool(request.user and request.user.has_perm('ticket.view_comments')) 


class IsAllowToViewAttachs(BasePermission):
    
    def has_permission(self, request, view):
        return bool(request.user and request.user.has_perm('ticket.view_attach'))

        
class IsManagerOrMember(BasePermission):

    def get_department_id(self, request):

        return int(request.parser_context['kwargs']['pk'])


    def get_status(self, request, queryset):
        
        department_id = self.get_department_id(request)

        is_manager = bool(queryset.filter(manager_id_id = request.user.id,
                                          id = department_id
                                         )
                         )

        if is_manager:
            return True  

        is_member = bool(list(filter(lambda query: query['users'] == request.user.id,
                             queryset.values('users').filter(id = department_id)
                            )
                     )
                )

        if is_member:
            return True

        return False

     
    def _queryset(self, view):
        assert hasattr(view, 'get_queryset') \
            or getattr(view, 'queryset', None) is not None, (
            'Cannot apply {} on a view that does not set '
            '`.queryset` or have a `.get_queryset()` method.'
        ).format(self.__class__.__name__)

        if hasattr(view, 'get_queryset'):
            queryset = view.get_queryset()
            assert queryset is not None, (
                '{}.get_queryset() returned None'.format(view.__class__.__name__)
            )
            return queryset
        return view.queryset

    
    def has_permission(self, request, view):
        
        if getattr(view, '_ignore_model_permissions', False):
            return True

        if not request.user or (
        not request.user.is_authenticated):
            return False

        if request.user and request.user.is_staff:
            return True

        queryset = self._queryset(view)
        
        return self.get_status(request, queryset)