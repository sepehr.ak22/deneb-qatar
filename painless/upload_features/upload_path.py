from datetime import (
    datetime,
    date
)

def images_upload_to(instance, filename):
    
    return '{app_name}/{username}/images/{date}/{filename}'.format(
            app_name = instance.__class__.__name__.lower(),
            username=instance.user.username,
            filename=filename,
            date = datetime.today().strftime('%Y-%m-%d')
        )


def videos_upload_to(instance, filename):
    
    return '{app_name}/{username}/videos/{date}/{filename}'.format(
            app_name = instance.__class__.__name__.lower(),
            username=instance.application.user.username,
            filename=filename,
            date = datetime.today().strftime('%Y-%m-%d')
        )


def blog_upload_to(instance, filename):
    
    return '{app_name}/{username}/{date}/{filename}'.format(
            app_name = instance.__class__.__name__.lower(),
            username=instance.author.username,
            filename=filename,
            date = datetime.today().strftime('%Y-%m-%d')
        )

def upload_to(instance, filename):
    
    return '{app_name}/{username}/{date}/{filename}'.format(
            app_name = instance.__class__.__name__.lower(),
            username=instance.user.username,
            filename=filename,
            date = datetime.today().strftime('%Y-%m-%d')
        )

def user_directory_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.user.email, filename)

def date_directory_path(instance, filename):
    today = date.today()
    return f'{today.year}/{today.month}/{today.day}/{filename}'
